var shopId = 1;

$(function(){
		setStartTime();
		setElement2();
		setInterval(time2, 1000);
		getDvcIdList();
	});
	
	
	function getDvcIdList(){
		var url = ctxPath + "/chart/getBarChartDvcId.do";
		
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));

		var sDate = year + "-" + month + "-" + day;
		var eDate = year + "-" + month + "-" + day + " 23:59:59";
		
		var param = "sDate=" + sDate + 
					"&eDate=" + eDate + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dvcId;
				for(var i = 0; i <=21; i++){
					drawBarChart2("status2_" + i, json[i].name);	
					getStatusChart2(json[i].dvcId, i);
				}
			}
		});
	};
	function getStatusChart2(dvcId, idx){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		var today = year + "-" + month + "-" + day + "%20" + hour + ":" + minute + ":" + second;
		
		var url = ctxPath + "/chart/getTimeChart.do";
		var param = "dvcId=" + dvcId + 
					"&targetDateTime=" + today;
		
		setInterval(function (){
			var minute = String(new Date().getMinutes());
			if(minute.length!=1){
				minute = minute.substr(1,2);
			};
			
			if(minute==2 && eval("dvcMap_" + idx).get("initFlag") || minute==2 && typeof(eval("dvcMap_" + idx).get("initFlag")=="undefined")){
				getStatusChart2(dvcId, idx);
				console.log("init")
				eval("dvcMap_" + idx).put("initFlag", false);
				eval("dvcMap_" + idx).put("currentFlag", true);
			}else if(minute!=2){
				eval("dvcMap_" + idx).put("initFlag", true);
			};
		}, 1000 * 10);
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				eval("dvcMap_" + idx + " = new JqMap();");
				
				if(data==null || data==""){
					eval("dvcMap_" + idx).put("noSeries", true);
					getCurrentDvcStatus2(dvcId, idx);
					return;
				}else{
					eval("dvcMap_" + idx).put("noSeries", false);
				};
				
				var json = $.parseJSON(data);
			
				var status = $("#status2_" + idx).highcharts();
				var options = status.options;
				
				options.series = [];
				//options.title = null;
				
				var flag = true;
				$(json).each(function (i, data){
					var bar = data.data[0].y;
					var startTime = data.data[0].startTime;
					var endTime = data.data[0].endTime;
					var color = eval(data.color);
					
					if(flag){
						options.series.push({
					        data: [{
					        		y : Number(200),
					        		segmentColor : color
					        	}],
					    });	
					}else{
						options.series[0].data.push({
							y : Number(200),
							segmentColor : color
						});
					};
					flag =  false;
				});  
				
				status = new Highcharts.Chart(options);
				getCurrentDvcStatus2(dvcId, idx);
			}
		});
	};
	
	function getCurrentDvcStatus2(dvcId, idx){
		var url = ctxPath + "/chart/getCurrentDvcData.do";
		var param = "dvcId=" + dvcId;
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var chartStatus = data.chartStatus;
				var name = data.name;
				
				var status = $("#status2_" + idx).highcharts();
				var options = status.options;
				
				if(eval("dvcMap_" + idx).get("currentFlag") || typeof(eval("dvcMap_" + idx).get("currentFlag"))=="undefined"){
					if(eval("dvcMap_" + idx).get("noSeries")){
		      			options.series = [];
		      			options.series.push({
					        data: [{
					        		y : Number(200),
					        		segmentColor : slctChartColor(chartStatus) 
					        	}],
					    });
		      		}else{
		      			options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : slctChartColor(chartStatus)
			  			});		      			
		      		};

		      		var now = options.series[0].data.length;
					var blank = 144 - now;
					
					for(var i = 0; i <= blank; i++){
						options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : "rgba(0,0,0,0.0)"
			  			});	
					};
					
					status = new Highcharts.Chart(options);
					eval("dvcMap_" + idx).put("currentFlag", false);
				};
				setTimeout(function (){
					getCurrentDvcStatus2(dvcId, idx);
				}, 3000);
				$("#dvcName" + idx).html(name);
			}
		});	
	};
	
	function slctChartColor(status){
		var color;
		if(status.toLowerCase()=="in-cycle"){
			color = colors[0];
		}else if(status.toLowerCase()=="wait"){
			color = colors[1];
		}else if(status.toLowerCase()=="alarm"){
			color = colors[2];
		}else if(status.toLowerCase()=="no-connection"){
			color = colors[3];
		};
		
		return color;
	};
	
	function setStartTime(){
		var url = ctxPath + "/chart/getStartTime.do"
		
		$.ajax({
			url :url,
			dataType :"text",
			type : "post",
			success : function(data){
				var startTime = Number(data);
				for(var i = 0; i<=24; i++){
					if(i+startTime>24){
						startTime -= 24;
					};
					
					timeLabel.push(i+startTime);
					for(var j = 0; j < 5; j++){
						timeLabel.push(0);	
					};
				};					
			}
		});
	};
	
	var timeLabel = [];
	
	function drawBarChart2(id, name){
		var fontColor = "white;"
			if(name=="NB13" || name=="NB14W"){
				fontColor = "black";
			}
		
		var perShapeGradient = {
	            x1: 0,
	            y1: 0,
	            x2: 1,
	            y2: 0
	        };
	        colors = Highcharts.getOptions().colors;
	        colors = [{
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#148F01'],
	                [1, '#1ABB02']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#C7C402'],
	                [1, '#F5F104']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#ff0000'],
	                [1, '#FF2626']
	                ]
	           },{
		            linearGradient: perShapeGradient,
		            stops: [
		                [0, '#8c9089'],
		                [1, '#A9ADA6']
		                ]}
	        ]
	         
		var options = {
			chart : {
				
				type : 'coloredarea',
				backgroundColor : 'rgba(255, 255, 255, 0)',
				height : $("#mainTable").height()*0.9/11,
				marginTop: -50
			},
			credits : false,
			exporting: false,
			title : {
				text :name,
				align :"left",
				y:5,
				x:5,
				style : {
					color : "white",
					fontSize: getElSize(10),
					fontWeight : "bold"
				}
			},
			yAxis : {
				labels : {
					enabled : false,
				},
				title : {
					text : false,
	                rotation: 0,
	                "textAlign": 'right',
					style : {
						color : "white"
					}
				},
			},
			xAxis:{
		           categories:timeLabel,
		            labels:{
		               
		                 formatter: function () {
			                        	var val = this.value
			                        	if(val==0){
			                        		val = "";
			                        	};
			                        	return val;   
			                        },
			                        style :{
			    	                	color : fontColor,
			    	                	fontSize : getElSize(10),
			    	                },
		            }
		        },
		       
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '10px',
				},
				enabled : false
			}, 
			plotOptions: {
			    line: {
			        marker: {
			            enabled: false
			        }
			    },
			    series : {
			    	animation : false
			    }
			},
			legend : {
				enabled : false
			},
			series: []
		}

	   	$('#' + id).highcharts(options);
	};
	
	function time2(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date2").html(month + " / " + date_ + " (" + day + ")");
		$("#time2").html(hour + " : " + minute + " : " + second);
	};
	
	function setElement2(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#title_main2").css({
			"width": getElSize(500)
		});
		
		$("#title_main2").css({
			"left" : width/2*0.5 - $("#title_main2").width()/2,
			"top" : (getElSize(25))
		});
		
		$("#title_left2").css({
			"width" : getElSize(150)
		});
		
		$("#title_left2").css({
			"left" : getElSize(25),
			"top" : (getElSize(25))
		});

		
		$("#title_right2").css({
			"color" : "white",
			"position" : "absolute",
			"font-weight" : "bolder",
			"font-size" : getElSize(20),
			"top" : $("#container").offset().top + (getElSize(40))
		});

		$("#title_right2").css({
			"left" : contentWidth/2 - $("#title_right").width() - getElSize(25)        
		});
	
		$("#time2").css({
			"right" : getElSize(25),
			"top" : (getElSize(85)),
			"font-size" : getElSize(15)
		});
		
		$("#date2").css({
			"right" : getElSize(150),
			"top" : (getElSize(85)),
			"font-size" : getElSize(15)
		});
		
		$("hr").css({
			"left" : width/2 - $("hr").width()/2,
			"margin-top": getElSize(120)
		});
		
		$("#mainTable").css({
			"left" : width/2*0.5 - $("#mainTable").width()/2
		});
		
		$(".status").css({
			"width" : contentWidth*0.45 * 0.5,
			"height" : contentHeight*0.8/11 * 0.5
		});
		
		
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};