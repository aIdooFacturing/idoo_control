<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
 <script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script> 
<!-- <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script> -->

<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
 <script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script> 
<!-- <script type="text/javascript" src="https://code.highcharts.com/modules/solid-gauge.js"></script>  -->
<script src="${ctxPath }/js/chart/solid-gauge.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	
	var dvcIdx = 0;
	var first = true;
	function getDvcList(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		
		var url =  ctxPath + "/chart/getDvcNameList.do";
		var param = "shopId=" + shopId + 
					"&line=ALL" + 
					"&sDate=" + today + 
					"&eDate=" + today + " 23:59:59" + 
					"&fromDashboard=" + fromDashboard;
		
		dvcArray = [];
		
		$.ajax({
			url : url,
			data :param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var list = "<option value='-1'>Auto</option>";
				
				$(json).each(function(idx, data){
					dvcArray.push(data.dvcId)
					list += "<option value='" + data.dvcId + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";					
				});

				$("#dvcId").html(list);
				
				$("#dvcId").css({
					"font-size" : getElSize(70),
					//"margin-bottom" : getElSize(10),
					"width" : getElSize(500),
					"-webkit-appearance": "none",
				    "-moz-appearance": "none",
			    	"appearance": "none",
			    	//"margin-top" : getElSize(100)
				});
				
				if(first){
					//localstorage에 저장 된 dvcid get
					for(var i = 0; i < dvcArray.length; i++){
						if(selectedDvcId == dvcArray[i]){
							dvcIdx = i;
						}
					}
					
					first = false;
				}
				
				dvcId = dvcArray[dvcIdx];
				getDvcData()
				$("#dvcId").val(dvcId);
				
				dvcIdx++;
				if(dvcIdx>=dvcArray.length) dvcIdx=0;
				
				//getDvcId();
				getCurrentDvcStatus(dvcId)
			}
		});
	};
	
	function getCurrentDvcStatus(dvcId){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var today = year + "-" + month + "-" + day;
		
		var url = "${ctxPath}/chart/getCurrentDvcData.do";
		var param = "dvcId=" + dvcId + 
					"&workDate=" + today;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var chartStatus = data.chartStatus;
				var type = data.type;
				var progName = data.lastProgramName;
				var progHeader = data.lastProgramHeader;
				var name = data.name;
				var inCycleTime = data.inCycleTime;
				var cuttingTime = data.cuttingTime;
				var waitTime = data.waitTime;
				var alarmTime = data.alarmTime;
				var noConTime = data.noConnectionTime;
				var spdLoad = data.spdLoad;
				var feedOverride = data.feedOverride;
				var opRatio = data.opRatio;
				var cuttingRatio = data.cuttingRatio;
				var alarm = data.alarm;
				var lastProgramName = data.lastProgramName;
				var lastProgramHeader = data.lastProgramHeader;
				
				if(lastProgramName==null)lastProgramName = "-"
				if(lastProgramHeader==null)lastProgramHeader = "-"
					
				var prgName = "[" + lastProgramName + "] " + lastProgramHeader;
				
				$("#programName").html(prgName).css({
					"color" : "white",
					"font-size" :getElSize(70),
					"position" : "absolute",
					"z-index" : 9,
					"top" : getElSize(250) + marginHeight,
					"left" : getElSize(1200) + marginWidth
				});
				
				
				$("#program_div").html(prgName).css({
					"color" : "white",
					"font-size" :getElSize(50),
					"margin" : getElSize(20) + "px"
				});
				
				if(data.chartStatus=="null"){
					name = window.sessionStorage.getItem("name");
				}
				
				
				//spd, feed
				$("#opratio_gauge").empty();
				$("#opratio_gauge").circleDiagram({
					textSize: getElSize(50), // text color
					percent : Math.round(opRatio) + "%",
					size: getElSize(200), // graph size
					borderWidth: getElSize(20), // border width
					bgFill: "#353535", // background color
					frFill: "#0080FF", // foreground color
					//font: "serif", // font
					textColor: '#0080FF' // text color
				});
				
				$("#cutting_gauge").empty();
				$("#cutting_gauge").circleDiagram({
					textSize: getElSize(50), // text color
					percent : Math.round(cuttingRatio) + "%",
					size: getElSize(200), // graph size
					borderWidth: getElSize(20), // border width
					bgFill: "#353535", // background color
					frFill: "#0080FF", // foreground color
					//font: "serif", // font
					textColor: '#0080FF' // text color
				});
						
				
				$("#opratio_span").html("${op_ratio}").css({
					"top" : getElSize(1780),
					"left" :$("#opratio_gauge").offset().left + ($("#cutting_gauge").width()/2) - ($("#opratio_span").width()/2)- marginWidth
				});
				
				$("#cuttingratio_span").html("${cuttingratio}").css({
					"top" : getElSize(1780),
					"left" :$("#cutting_gauge").offset().left + ($("#cutting_gauge").width()/2) - ($("#cuttingratio_span").width()/2)- marginWidth
				});
				
				
				if(String(type).indexOf("IO")!=-1){
					//$("#progHeader" + idx).append("I/O Logik");
				};
				
				
				$("#cutSpan").html((cuttingTime/60/60).toFixed(1));
				$("#inCycleSpan").html((inCycleTime/60/60).toFixed(1));
				$("#waitSpan").html((waitTime/60/60).toFixed(1));
				$("#alarmSpan").html((alarmTime/60/60).toFixed(1));
				$("#noConnSpan").html((noConTime/60/60).toFixed(1));
				
				
				getDetailData()
				setInterval(function(){
					getCurrentDvcStatus(dvcId)					
				}, 1000*60*10)
			}
		});
	};
	
	function getDvcData(){
		//getMachineName();
		/* getDetailData();
		getAlarmList();
		getRapairDataList(); */
		getStartTime();
		
		//setTimeout(getDvcData, 5000);
	};
	
	var handle = 0;
	var selectedDvcId;
	function drawGauge(){
		$("#gauge").css({
			"width" : getElSize(900),
			"height" : getElSize(380)
		})
		
		var gaugeOptions = {

			    chart: {
			        type: 'solidgauge',
			        backgroundColor : "rgba(0,0,0,0)",
			        marginBottom : 0,
			        marginTop : -getElSize(460)
			        
			    },

			    
			    title: null,

			    pane: {
			        center: ['50%', '100%'],
			        size: '90%',
			        startAngle: -90,
			        endAngle: 90,
			        background: {
			            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
			            innerRadius: '60%',
			            outerRadius: '100%',
			            shape: 'arc'
			        }
			    },

			    tooltip: {
			        enabled: false
			    },

			    // the value axis
			    yAxis: {
			        stops: [
			            /* [0.1, '#55BF3B'], // green
			            [0.5, '#DDDF0D'], // yellow
			            [0.9, '#DF5353'] // red */
			            [1, '#55BF3B'], // green
			        ],
			        lineWidth: 0,
			        minorTickInterval: null,
			        tickAmount: 2,
			        title: {
			            y: -70
			        },
			        labels: {
			            y: 16
			        }
			    },

			    plotOptions: {
			        solidgauge: {
			            dataLabels: {
			                y: 5,
			                borderWidth: 0,
			                useHTML: true
			            }
			        }
			    }
			};

			// The speed gauge
			$('#gauge').highcharts(Highcharts.merge(gaugeOptions, {
			//var chartSpeed = Highcharts.chart('gauge', Highcharts.merge(gaugeOptions, {
			    yAxis: {
			    	min: 0,
					max: 100,
			        title: {
			            text: ''
			        }
			    },

			    credits: {
			        enabled: false
			    },

			    series: [{
			        name: 'Speed',
			        data: [0],
			        dataLabels: {
			            format: '<div style="text-align:center"><font style="font-size:25px; color:' +
			                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'blue') + '">{y}%</font>'
			        },
			        tooltip: {
			            valueSuffix: ' km/h'
			        }
			    }]

			}));
	};
	
	function changeDateVal(){
	console.log("changed")
		var now = new Date(); 
		var todayAtMidn = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		
		var date = new Date();
		
		var year = date.getFullYear();
		var month = date.getMonth()+1;
		var day = date.getDate();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		// Set specificDate to a specified date at midnight.
		
		var selectedDate = $("#today").val();
		var s_year = selectedDate.substr(0,4);
		var s_month = selectedDate.substr(5,2);
		var s_day = selectedDate.substr(8,2);

		var specificDate = new Date(s_month + "/" + s_day + "/" + s_year);
		
		var time = year + "-" + month + "-" + day;
		
		var today = getToday().substr(0,10);
		if(todayAtMidn.getTime()<specificDate.getTime()){
			alert("오늘 이후의 날짜는 선택할 수 없습니다.");
			$("#today").val(today);
			return;
		};
		
		
		if(today==$("#today").val()){
			var date = new Date();
			var year = date.getFullYear();
			var month = addZero(String(date.getMonth() + 1));
			var day = addZero(String(date.getDate()));
			var hour = date.getHours();
			var minute = addZero(String(date.getMinutes())).substr(0,1);
			
			
			if(hour>startHour || (hour>=startHour &&minute >= startMinute)){
				day = addZero(String(new Date().getDate()+1));
			};
			
			
			selectedDate = year + "-" + month + "-" + day;
			
			
		}
		
		console.log(selectedDate)
		
		drawBarChart("timeChart", selectedDate);
		getDetailData()

	};
	
	function upDate(){
		var $date = $("#today").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() + 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#today").val(today);
		changeDateVal();
	};
	
	function downDate(){
		var $date = $("#today").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() - 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#today").val(today);
		changeDateVal();
	};
	
	
	
	$(function(){
		$("#today").change(changeDateVal);
		$("#up").click(upDate);
		$("#down").click(downDate);
		
		//drawGauge();
		selectedDvcId = window.localStorage.getItem("dvcId");
		//if(selectedDvcId==null) selectedDvcId =96; 
		
		createNav("monitor_nav", 1)
		getDvcList();
		time();
		
		$("#dvcId").change(function(){
			dvcId = $("#dvcId").val(); 
			if(this.value==-1){
				rotate_flag = true;	
			}else{
				rotate_flag = false;
				getDvcData();
				getDetailData();
				getCurrentDvcStatus(dvcId);
			}
		});
		
		
	
		setDivPos();
		
		/* window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10); */
		
		
		chkBanner();
	});
	
	var startTimeLabel = new Array();
	var startHour, startMinute;
	
	function getStartTime(){
		var url = ctxPath + "/chart/getStartTime.do";
		var param = "shopId=" + shopId;;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				startHour = data.split("-")[0]
				startMinute = data.split("-")[1]

				
				var date = new Date();
				var year = date.getFullYear();
				var month = addZero(String(date.getMonth() + 1));
				var day = addZero(String(date.getDate()));
				var hour = date.getHours();
				var minute = addZero(String(date.getMinutes())).substr(0,1);
				
				
				if(hour>startHour || (hour>=startHour &&minute >= startMinute)){
					day = addZero(String(new Date().getDate()+1));
				};
				
				
				var today = year + "-" + month + "-" + day;
				
				drawBarChart("timeChart", today);
			}, error : function(e1,e2,e3){
				console.log(e1,e2,e3)
			}
		});	
	};
	
	function drawBarChart(id, today) {
		var m0 = "",
			m02 = "",
			m04 = "",
			m06 = "",
			m08 = "",
			
			m1 = "";
			m12 = "",
			m14 = "",
			m16 = "",
			m18 = "",
			
			m2 = "";
			m22 = "",
			m24 = "",
			m26 = "",
			m28 = "",
			
			m3 = "";
			m32 = "",
			m34 = "",
			m36 = "",
			m38 = "",
			
			m4 = "";
			m42 = "",
			m44 = "",
			m46 = "",
			m48 = "",
			
			m5 = "";
			m52 = "",
			m54 = "",
			m56 = "",
			m58 = "";
		
		var n = Number(startHour);
		if(startMinute!=0) n+=1;
		
		for(var i = 0, j = n ; i < 24; i++, j++){
			eval("m" + startMinute + "=" + j);
			
			startTimeLabel.push(m0);
			startTimeLabel.push(m02);
			startTimeLabel.push(m04);
			startTimeLabel.push(m06);
			startTimeLabel.push(m08);
			
			startTimeLabel.push(m1);
			startTimeLabel.push(m12);
			startTimeLabel.push(m14);
			startTimeLabel.push(m16);
			startTimeLabel.push(m18);
			
			startTimeLabel.push(m2);
			startTimeLabel.push(m22);
			startTimeLabel.push(m24);
			startTimeLabel.push(m26);
			startTimeLabel.push(m28);
			
			startTimeLabel.push(m3);
			startTimeLabel.push(m32);
			startTimeLabel.push(m34);
			startTimeLabel.push(m36);
			startTimeLabel.push(m38);
			
			startTimeLabel.push(m4);
			startTimeLabel.push(m42);
			startTimeLabel.push(m44);
			startTimeLabel.push(m46);
			startTimeLabel.push(m48);
			
			startTimeLabel.push(m5);
			startTimeLabel.push(m52);
			startTimeLabel.push(m54);
			startTimeLabel.push(m56);
			startTimeLabel.push(m58);
			
			if(j==24){ j = 0}
		};

		var perShapeGradient = {
			x1 : 0,
			y1 : 0,
			x2 : 1,
			y2 : 0
		};
		colors = Highcharts.getOptions().colors;
		colors = [ {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, '#8C9089' ], [ 1, '#8C9089' ] ]
		}, ]

		var height = window.innerHeight;

		var options = {
			chart : {
				type : 'coloredarea',
				backgroundColor : 'rgba(0,0,0,0)',
				height : getElSize(200),
				marginTop : -getElSize(300),
		    	marginRight : getElSize(50),
		    	marginLeft : getElSize(50)
			},
			credits : false,
			title : false,
			xAxis : {
				categories : startTimeLabel,
				labels : {
					step: 1,
					formatter : function() {
						var val = this.value

						return val;
					},
					style : {
						color : "white",
						fontSize : getElSize(30),
						fontWeight : "bold"
					},
				}
			},
			yAxis : {
				labels : {
					enabled : false,
				},
				title : {
					text : false
				},
			},
			tooltip : {
				enabled : false
			},
			plotOptions : {
				line : {
					marker : {
						enabled : false
					}
				}
			},
			legend : {
				enabled : false
			},
			series : []
		};

		$("#" + id).highcharts(options);

		var status = $("#" + id).highcharts();
		var options = status.options;

		options.series = [];
		options.title = null;
		options.exporting = false;
		
		getTimeData(options, today);
		
		///////////////////////// demo data
		
		
//		options.series.push({
//			data : [ {
//				y : Number(20),
//				segmentColor : "red"
//			} ],
//		});
	//	
	//	
//		for(var i = 0; i < 719; i++){
//			var color = "";
//			var n = Math.random() * 10;
//			if(n<=5){
//				color = "green";
//			}else if(n<=8){
//				color = "yellow";
//			}else {
//				color = "red";
//			}
//			options.series[0].data.push({
//				y : Number(20),
//				segmentColor : color
//			});
//		};
	//	
//		status = new Highcharts.Chart(options);
		
		
		
		////////////////////////////////
		
		setInterval(function(){
			var cMinute = String(addZero(new Date().getMinutes())).substr(0,1);
			if(targetMinute!=cMinute){
				targetMinute = cMinute;
				drawBarChart(id, today);
			};
		},5000)
	};
	
	var targetMinute = String(addZero(new Date().getMinutes())).substr(0,1);
	
	function getTimeData(options, today){
		var url = ctxPath + "/chart/getTimeData.do";
		
		var param = "workDate=" + today + 
					"&dvcId=" + dvcId;
		
		console.log(param)
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			data : param,
			success : function(data){
				console.log(data)
				var json = data.statusList;
				
				var color = "";
				
				var status = json[0].status;
				if(status=="IN-CYCLE"){
					color = "green"
				}else if(status=="WAIT"){
					color = "yellow";
				}else if(status=="ALARM"){
					color = "red";
				}else if(status=="NO-CONNECTION"){
					color = "#A0A0A0";
				};
				
				spdLoadPoint = [];
				spdOverridePoint = [];
				
				var blank;
				var f_Hour = json[0].startDateTime.substr(11,2);
				var f_Minute = json[0].startDateTime.substr(14,2);
				
				var startN = 0;
				if(f_Hour==startHour && f_Minute==(startMinute*10)){
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : color
						} ],
					});
				}else{
					if(f_Hour>=Number(startHour)){
						startN = (((f_Hour*60) + Number(f_Minute)) - ((startHour*60) + (startMinute*10)))/2; 
					}else{
						console.log(f_Hour, f_Minute)
						startN = ((24*60) - ((startHour*60) + (startMinute*10)))/2;
						startN += (f_Hour*60/2) + (f_Minute/2);
					};
					
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : "#A0A0A0"
						} ],
					});
						
					for(var i = 0; i < startN-1; i++){
						options.series[0].data.push({
							y : Number(20),
							segmentColor : "#A0A0A0"
						});
						spdLoadPoint.push(Number(0));
						spdOverridePoint.push(Number(0));
					};
				};
				
				
				$(json).each(function(idx, data){
					spdLoadPoint.push(Number(data.spdLoad));
					spdOverridePoint.push(Number(data.spdOverride));
					
					if(data.status=="IN-CYCLE"){
						color = "#A3D800"
					}else if(data.status=="WAIT"){
						color = "#FF9100";
					}else if(data.status=="ALARM"){
						color = "#EC1C24";
					}else if(data.status=="NO-CONNECTION"){
						color = "#A0A0A0";
					};
					
					options.series[0].data.push({
						y : Number(20),
						segmentColor : color
					});
				});
				
				console.log(startN)
				console.log(json.length+startN)
				for(var i = 0; i < 719-(json.length+startN); i++){
					options.series[0].data.push({
						y : Number(20),
						segmentColor : "rgba(0,0,0,0)"
					});
					spdLoadPoint.push(Number(-10));
					spdOverridePoint.push(Number(-10));
				};
				
				status = new Highcharts.Chart(options);
				getSpdFeed();
				
				//drawLabelPoint();
			},error : function(e1,e2,e3){
			}
		});
	};
	
	var spdLoadPoint = [];
	var spdOverridePoint = [];
	
	function getSpdFeed(){
		$('#spdFeed').highcharts({
	        chart: {
	            type: 'line',
	            backgroundColor : "rgba(0,0,0,0)",
	            height : getElSize(380),
	        },
	        exporting : false,
	        credits : false,
	        title: {
	            text: null
	        },
	        subtitle: {
	            text: null
	        },
	        legend : {
	        	enabled : false,
	        	itemStyle : {
	        			color : "white",
	        			fontWeight: 'bold'
	        		}
	        },
	        xAxis: {
	            //categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	        	labels : {
	        		enabled : false
	        	}
	        },
	        yAxis: {
	        	min : 0,
	        	gridLineWidth: 1,
				minorGridLineWidth: 0,
	            title: {
	                text: null
	            },
	            labels : {
	            	style : {
	            		color : "white"
	            	}
	            }
	        },
	        plotOptions: {
	            line: {
	                dataLabels: {	
	                    enabled: false,
	                    color: 'white',
	                    style: {
	                        textShadow: false 
	                    }
	                },
	                enableMouseTracking: false
	            }
	        },
	        series: [{
	            name: 'Spindle Load',
	            color : 'yellow',
	            //data: spdLoadPoint
	            data : spdLoadPoint,
	            lineWidth : getElSize(10),
	            marker : {
                    enabled : false,
                },
	        }, {
	            name: 'Spindle Override',
	            color : 'red',
	            data: spdOverridePoint,
	            lineWidth : getElSize(10),
	            marker : {
                    enabled : false,
                },
	        }]
	    });	
		
		spdChart = $('#spdFeed').highcharts();
		$("#legend").css("display", "inline");
	};
	
	function drawPrdctChart(ratio){
		$("#target_ratio").html(Math.round(ratio) + "%").css({
			"color" : "#0080FF",
			"font-size" : getElSize(100),
			"right" : getElSize(30),
			"top" : getElSize(850)
		});
		
		$('#prdctChart').highcharts({
		    chart: {
		    	backgroundColor : "rgba(0,0,0,0)",
		    	height : getElSize(230),
		    	marginRight : getElSize(50),
		    	marginLeft : getElSize(50),
		        type: 'bar'
		    },
		    credits : false,
		    exporting : false,
		    title: {
		        text: null
		    },
		    xAxis: {
		    	/* gridLineWidth:0,
		    	lineWidth: 0,*/
		    	tickWidth: 0, 
				labels: {
		       		enabled: false
		    	},    
		    },
		    yAxis: {
			    gridLineWidth: 0,
				minorGridLineWidth: 0,
		        min: 0,
		        max :100,
				labels : {
					style : {
						color : "#7A7B7C",
						fontSize :getElSize(30)
					}
		        },
		        title: {
		            text: null
		        }
		    },
		    tooltip : {
				enabled : false
			},
		    legend: {
		        reversed: true,
		        enabled : false
		    },
		    plotOptions: {
		        series: {
		            stacking: 'normal',
		            groupPadding: 0,
		            pointPadding: 0,
		            borderWidth: 0
		        }
		    },
		    /* series: [{
		    		color : "#BBBDBF",
		        name: 'target',
		        data: [10]
		    },{
		    		color : "#A3D800",
		        name: 'goal',
		        data: [10]
		    }] */
		    series: [{
	    		color : "#A3D800",
	        name: 'goal',
	        data: [Math.round(ratio)]
	    }]
		});			
	};
	
	
	function getDetailData(){
		var url = ctxPath + "/chart/getDetailBlockData.do";
		var param = "dvcId=" + dvcId + 
					"&sDate=" + $("#today").val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var alarm = "";
				$(json).each(function(idx, data){
					var hour = new Date().getHours()-8;
					if(hour==0) hour=1;
					 
					var sign = "";
					
					
					if(String(data.type).indexOf("IO") != -1){
						$("#cutting_gauge, #cuttingratio_span").remove()
					} 
					
					
					
					
					$("#cylCnt").html(Math.round(data.lastFnPrdctNum/data.prdctPerCyl));
					$("#prdctPerCyl").html(data.prdctPerCyl);
					$("#daily_target_cycle").html(data.tgCnt);
					$("#complete_cycle").html(data.lastFnPrdctNum);
					$("#daily_avg_cycle_time").html(Number(data.LastAvrCycleTime/60).toFixed(1));
					$("#daily_length").html(Number(data.prdctPerHour).toFixed(1));
					$("#feedOverride").html(Number(data.feedOverride));
					$("#spdLoad").html(Number(data.spdLoad));
					
					//var remainCnt = data.remainCnt;
					var remainCnt = data.tgCnt - (data.lastFnPrdctNum);
					var color = "";
					if(Number(remainCnt)>0){
						sign = "-";
						color = "red"
					}else{
						sign = "+";
						remainCnt = Math.abs(remainCnt);
						color = "blue";
					};
					
					$("#downValue").html(sign + remainCnt).css({
						"font-size" : getElSize(70),
						"color" : color
					})
					
					
					//일 생산 현황
					//drawPrdctChart(data.lastFnPrdctNum / data.tgCnt * 100);
					/* var prdctChart = $("#gauge").highcharts().series[0].points[0];
					prdctChart.update(Number(Number(data.lastFnPrdctNum / data.tgCnt * 100).toFixed(1))); */
					
					
					$("#gauge").css({
					}).empty();
					
					$("#gauge").circleDiagram({
						textSize: getElSize(70), // text color
						percent : Number(Number(data.lastFnPrdctNum / data.tgCnt * 100).toFixed(1)) + "%",
						size: getElSize(300), // graph size
						borderWidth: getElSize(50), // border width
						bgFill: "#353535", // background color
						frFill: "#0080FF", // foreground color
						//font: "serif", // font
						textColor: '#0080FF' // text color
					});
					
					
					//data.lastAlarmCode + " - " + data.lastAlarmMsg;
					var alarm1, alarm2, alarm3;
					if(data.ncAlarmNum1==""){
						alarm1 = "";
					}else{
						alarm1 = data.ncAlarmNum1 + " - " +  decodeURIComponent(data.ncAlarmMsg1).replace(/\+/gi, " ") + "<br>";
					};
					if(data.ncAlarmNum2==""){
						alarm2 = "";
					}else{
						alarm2 = data.ncAlarmNum2 + " - " +  decodeURIComponent(data.ncAlarmMsg2).replace(/\+/gi, " ") + "<br>";
					};
					if(data.ncAlarmNum3==""){
						alarm3 = "";
					}else{
						alarm3 = data.ncAlarmNum3 + " - " +  decodeURIComponent(data.ncAlarmMsg3).replace(/\+/gi, " ") + "<br>";
					};
					
					
					alarm += alarm1 +
							alarm2 +
							alarm3; 
							
					if(data.status=="ALARM" && alarm == ""){
						//console.log("get")
						alarm = "[장비 확인 필요]";
					}
					
					if(alarm!=""){
						alarm = "<font style='color:red' id='alarm_span'>-Alarm</font><br>" + alarm
					}
					
					$("#alarm_div").html(alarm).not("#alarm_span").css({
						"color" : "white",
						"padding" : getElSize(50),
						"margin" : getElSize(20) + "px"
					});	
					
					var n = (data.lastFnPrdctNum)/data.tgCnt*100;
					//barChart.series[0].data[0].update(Number(Number(n).toFixed(1)));
				});
				
				
				$("#downValue").css({
					"position" : "absolute",
					"top" : getElSize(350)
				});

				$("#downValue").css({
					"right" : getElSize(2530),
				});
				
				//setTimeout(getDetailData, 5000)
			}
		});
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setDivPos(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(35),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$(".content").css({
			"height" : getElSize(400),
			"background-color" : "#191919",
			"color" : "white",
			"text-align" : "center",
			"font-size" : getElSize(170),
		});
		
		$("#prdctChart_td").css({
			"height" : getElSize(400),
			"background-color" : "#191919",
		});
		
		$("#timeChart_td").css({
			"height" : getElSize(700),
			"background-color" : "#191919"
		});
	
		$("#timeChart").css({
			"margin-top" : getElSize(50)	
		});
		
		$("#dvcId").css({
			"position" : "absolute",
			"font-size" : getElSize(70),
			"margin-top" : getElSize(25),
			"margin-left" : getElSize(30)
		});
		
		$("#statusChart").css({
			"width" : getElSize(750),
			"margin-top" : getElSize(150)
		});
		
		$("#legend").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"margin-left" : getElSize(1600),
			"display" : "none"
		});
		
		$("#spdLoadLine").css({
			"background-color" : "#F9FF00",			
			"width" : getElSize(90),
			"height" : getElSize(20)
		});
		
		$("#spdOvrrdLine").css({
			"background-color" : "#FF0000",
			"width" : getElSize(90),
			"height" : getElSize(20),
			"margin-left" : getElSize(50)
		});
		
		$("#cutTime, #inCycleTime, #waitTime, #alarmTime, #noConnTime").css({
			"color" : "black",
			"font-size" : getElSize(35)
		});
		
		//1043
		
		$("#cutTime").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(837),
			"left" : getElSize(960)
		});
		
		$("#inCycleTime").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(895),
			"left" : getElSize(960)
		});
		
		$("#waitTime").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(958),
			"left" : getElSize(960)
		});
		
		$("#alarmTime").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(1021),
			"left" : getElSize(960)
		});
		
		$("#noConnTime").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(1084),
			"left" : getElSize(960)
		});
		
		$("#cutSpan, #inCycleSpan, #waitSpan, #alarmSpan, #noConnSpan").css({
			"color" : "black",
			"font-size" : getElSize(50),
			"position" : "absolute"
		});
		
		$("#cutSpan").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(829),
			"left" : getElSize(1110)
		});
		
		$("#inCycleSpan").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(887),
			"left" : getElSize(1110)
		});
		
		$("#waitSpan").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(950),
			"left" : getElSize(1110)
		});
		
		$("#alarmSpan").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(1015),
			"left" : getElSize(1110)
		});
		
		$("#noConnSpan").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(1077),
			"left" : getElSize(1110)
		});
		
		$("#opratio_gauge").css({
			"margin-left" : getElSize(100),
			"margin-top" : getElSize(150)
		});
		
		$("#cutting_gauge").css({
			"margin-right" : getElSize(100),
			"margin-top" : getElSize(150)
		});
		$("img").css({
			"display" : "inline"
		})
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#date_table").css({
			"position" : "absolute",
			"top" :getElSize(200),
			"right" : getElSize(50)
		});
		
		$("#date_table button").css({
			"width" : getElSize(100),
			"height" : getElSize(60),
		});
		
		$("#today").val(getToday().substr(0,10)).css({
			"font-size" : getElSize(30) + "px",
			"height" : getElSize(60)
		})
		
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="programName"></div>
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right' style="display: none">
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/monitor_left.png" class='menu_left'  style="display: none">
				</td>
				<td >
					<select id="dvcId"></select>
					<img alt="" src="${ctxPath }/images/blue_right.png" class='menu_right' style="display: none">
				</td>
			</tr>
			<Tr>
				<Td>
					<span  class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table  style="width: 100%" id="main_table">
						<tr>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="dailyprdcttarget"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="dailyprdctcnt"></spring:message>	
								<span id="downValue"></span>
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="prdct_per_cycle"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="daily_cycle_cnt"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="daily_prdct_avrg_cycle_time"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="daily_prdct_cnt_per_hour"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								Feed Override	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								Spindle Load	
							</td>
						</tr>
						<tr>
							<td class='content' id="daily_target_cycle"></td>
							<td class='content' id="complete_cycle"></td>
							<td class='content' id="prdctPerCyl"></td>
							<td class='content' id="cylCnt"></td>
							<td class='content' id="daily_avg_cycle_time"></td>
							<td class='content' id="daily_length"></td>
							<td class='content' id="feedOverride"></td>
							<td class='content' id="spdLoad"></td>
						</tr>
						<tr>
							<TD class="title_span" colspan="2">
								<spring:message code="dailystackedstatus"></spring:message>	
							</TD>
							<TD class="title_span" colspan="3" >
								<spring:message code="prdct_status"></spring:message>	
							</TD>
							<TD class="title_span" colspan="3" >
								<spring:message code="proces_status"></spring:message>
							</TD>
						</tr>
						<Tr>
							<td rowspan="3" colspan="2" style="text-align: center; vertical-align: top;" >
								<span id="cutTime">Cut</span> <span id="cutSpan">0</span>
								<span id="inCycleTime">In-Cycle</span> <span id="inCycleSpan">0</span>
								<span id="waitTime">Wait</span> <span id="waitSpan">0</span>
								<span id="alarmTime">Alarm</span> <span id="alarmSpan">0</span>
								<span id="noConnTime">Off</span> <span id="noConnSpan">0</span>
								
								<img alt="" src="${ctxPath }/images/statusChart.png" id="statusChart">
								
								<div id="opratio_gauge" style="float: left;"></div>
								<div id="cutting_gauge" style="float: right;"></div>
								<span id="opratio_span"></span>
								<span id="cuttingratio_span"></span>
								
							</td>
							<td colspan="3" style="text-align: center; vertical-align: bottom;" id="prdctChart_td">
								<!-- <span id="target_ratio" ></span>
								<div id="prdctChart" ></div> -->
								<center>
									<div id="gauge"></div>
								</center> 
							</td>
							<td colspan="3" style="background-color: rgb(25,25,25); vertical-align: top;" >
								<div id="program_div"></div>
								<div id="alarm_div"></div>
							</td>
						</Tr>
						<tr>
							<TD class="title_span" colspan="6">
								<spring:message code="operation_status"></spring:message>	
								
								<table id="date_table">
									<tr>
										<!-- <td>
											<button id="up">⬆︎</button><button id="down">⬇︎</button>
										</td> -->
										<td>
											<input type="date" id="today" >		
										</td>
									</tr>
								</table>
							</TD>
						</tr>
						<tr>
							<td colspan="6" style="text-align: center; vertical-align: top;" id="timeChart_td">
								<div id="timeChart"></div>
								<div id="spdFeed"></div>
								<table id="legend">
									<Tr>
										<td><div id="spdLoadLine"></div></td>
										<Td>Spindle Load</Td>
										<td ><div id="spdOvrrdLine"></div></td>
										<Td>Spindle Override</Td>
									</Tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_blue.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
		</table>
	 </div>
	
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	