<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function getBanner(){
		var url = "${ctxPath}/chart/getBanner.do";
		var param = "shopId=" + shopId;
		

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				if(window.localStorage.getItem("banner")=="true"){
					$("#intro").html(data.msg).css({
						"color" : data.rgb,
						"right" : - window.innerWidth
					})
					
					$("#intro").html(data.msg).css({
						"right" : - $("#intro").width()
					})
				}
				
				bannerWidth = $("#intro").width() + getElSize(100); 
				$("#intro").width(bannerWidth);
				$("#banner").val(data.msg);
				$("#color").val(data.rgb);
				
				twinkle();
				bannerAnim();
			}
		});		
	}
	
	var twinkle_opct = false;
	function twinkle(){
		var opacity;
		if(twinkle_opct){
			opacity = 0;
		}else{
			opacity = 1;
		}
		$("#intro").css("opacity",opacity);
		
		twinkle_opct = !twinkle_opct;
		setTimeout(twinkle, 300)
	};
	
	var bannerWidth;
	function bannerAnim(){
		$("#intro").width(bannerWidth - getElSize(10));
		$("#intro").animate({
			"right" : window.innerWidth  - getElSize(100)
		},8000, function(){
			$("#intro").css("right" , - $("#intro").width())
			//$(this).remove();
			bannerAnim();
		});
	};
	
	function chkBanner(){
		if(window.localStorage.getItem("banner")=="true"){
			getBanner();
			$("#intro_back").css("display","block");
		}else{
			$("#intro").html("");
			$("#intro_back").css("display","none");
		}
	};
	
	function cancelBanner(){
		window.localStorage.setItem("banner", "false");
		$("#bannerDiv").css("z-index",-9);
		chkBanner();
	};
	
	function addBanner(){
		var url = "${ctxPath}/chart/addBanner.do";
		var param = "msg=" + encodeURIComponent($("#banner").val()) + 
					"&rgb=" + $("#color").val() + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				window.localStorage.setItem("banner", "true");
				location.reload();
				/* $("#bannerDiv").css("z-index",-9);
				chkBanner(); */
			}
		});
	};
	
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month);
	};
	
	var handle = 0;
	
	$(function(){
		getGroup();
		getGroupCode();
		$("#chkTy").html("<option value='ALL'>${total}</option>" + getCheckType());
		
		createNav("quality_nav", 1);
		
		setEl();
		setDate();	
		time();
		
		$("#home").click(function(){ location.href = "/iDOO_CONTROL/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"display" : "none",
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

	
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#contentDiv").css({
			"overflow" : "auto",
			"width" : $(".menu_right").width()
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function getGroup(){
		var url = "${ctxPath}/chart/getPrdNoList.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				var option;
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
				});
				
				$("#prdNo").html(option).val(json[0].prdNo).change(getDvcListByPrdNo);
				
				//getLeadTime();
				getDvcListByPrdNo();
			}
		});
	};
	
	function getDvcListByPrdNo(){
		var url = "${ctxPath}/chart/getDvcListByPrdNo.do";
		var param = "prdNo=" + $("#prdNo").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				var option;
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.dvcId + "'>" + decode(data.name) + "</option>"; 
				});
				
				$("#dvcId").html(option).val(json[0].dvcId);
				getChkStandardList();
			}
		});
	};

	var className = "";
	var classFlag = true;
	
	function getChkStandardList(){
		var url = "${ctxPath}/chart/getChkStandardList.do";
		var param = "prdNo=" + $("#prdNo").val() + 
					"&dvcId=" + $("#dvcId").val() + 
					"&chkTy=" + $("#chkTy").val()

					console.log(param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
		
				console.log(json)
				$(".contentTr").remove();
				var tr = "<tbody>";
				$(json).each(function(idx, data){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					tr = "<tr class='" + className + " contentTr' id='tr" + data.id + "'>" + 
								"<td>" + data.prdNo + "</td>" +
								"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" + 
								"<td id='chkTy" + data.id + "'><select style='font-size : " + getElSize(40) + "'>" + getCheckType() + "</select></td>" + 
								"<td id='attrTy" + data.id + "'>" + attrTy + "</td>" + 
								"<td><font>" + data.attrCd.substr(0,1) + "</font><input type='text' value='" + data.attrCd.substr(1) + "' size='6'></td>" + 
								"<td><input type='text' value='" + decodeURIComponent(data.attrNameKo).replace(/\+/gi, " ") + "'></td>" + 
								"<td><input id='dp" + data.id + "' type='text' value='" + data.dp + "' size='2'></td>" + 
								"<td><input id='unit" + data.id + "' type='text' value='" + data.unit + "' size='2'></td>" + 
								"<td><input type='text' value='" + decodeURIComponent(data.spec).replace(/\+/gi, " ") + "'></td>" + 
								"<td><input id='target" + data.id + "' type='text' value='" + data.target + "' size='6'></td>" + 
								"<td><input id='low" + data.id + "' type='text' value='" + data.low + "' size='6'></td>" + 
								"<td><input id='up" + data.id + "' type='text' value='" + data.up + "' size='6'></td>" + 
								"<td><input type='text' value='" + decodeURIComponent(data.measurer).replace(/\+/gi, " ") + "' size='8'></td>" + 
								"<td><select id='jsGroupCd" + data.id + "' style='font-size : " + getElSize(40) + "'>" + getGroupCode() + "</select></td>" + 
								"<td><input type='text' value='" + data.attrNameOthr + "'></td>" + 
								"<td><button onclick='chkDel(this)'>삭제</button></td>" + 
						"</tr>";
						
					$("#table2").append(tr);	
					$("#chkTy" + data.id + " select option[value=" + data.chkTy + "]").attr('selected','selected');
					$("#attrTy" + data.id + " select option[value=" + data.attrTy + "]").attr('selected','selected');
					
					if(data.attrTy==1){
						$("#dp" + data.id).val("").attr("disabled", true).css("background-color", "#929292");
						$("#unit" + data.id).val("").attr("disabled", true).css("background-color", "#929292");
						
						$("#target" + data.id).val("").attr("disabled", true).css("background-color", "#929292");
						$("#up" + data.id).val("").attr("disabled", true).css("background-color", "#929292");
						$("#low" + data.id).val("").attr("disabled", true).css("background-color", "#929292");
						$("#jsGroupCd" + data.id).attr("disabled", false).css("background-color", "#ffffff");
					}else{
						$("#jsGroupCd" + data.id).attr("disabled", true).css("background-color", "#929292");
					}
				});
				
				$("#table2").append("</tbody>");
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				setEl();
			}
		});
	};
	
	function getCheckType(){
		var url = "${ctxPath}/chart/getCheckType.do";
		var param = "codeGroup=INSPPNT";
		
		var option = "";
		
		$.ajax({
			url : url,
			data : param,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "' >" + decode(data.codeName) + "</option>"; 
				});
				
				chkTy = "<select>" + option + "</select>";
			}
		});
		
		return option;
	};

	var valid = true;
	var valueArray = [];
	function saveRow(){
		valid = true;
		valueArray = [];
		$(".contentTr").each(function(idx, data){
			if(valid){
				var obj = new Object();
				
				obj.id = data.id.substr(2);
				obj.prdNo = $("#prdNo").val();
				obj.dvcId = $("#dvcId").val();
				obj.chkTy = $(data).children("td:nth(2)").children("select").val();
				obj.attrTy = $(data).children("td:nth(3)").children("select").val();
				obj.attrCd = $(data).children("td:nth(4)").children("font").html() + $(data).children("td:nth(4)").children("input").val();
				obj.attrNameKo = $(data).children("td:nth(5)").children("input").val();
				obj.dp = $(data).children("td:nth(6)").children("input").val();
				obj.unit = $(data).children("td:nth(7)").children("input").val();
				obj.spec = $(data).children("td:nth(8)").children("input").val();
				obj.target = $(data).children("td:nth(9)").children("input").val();
				obj.low = $(data).children("td:nth(10)").children("input").val();
				obj.up = $(data).children("td:nth(11)").children("input").val();
				obj.measurer = $(data).children("td:nth(12)").children("input").val();
				obj.jsGroupCd = $(data).children("td:nth(13)").children("select").val();
				obj.attrNameOthr = $(data).children("td:nth(14)").children("input").val();
				
				valueArray.push(obj);
				
				var target = $(data).children("td:nth(9)").children("input").val();
				var min = $(data).children("td:nth(10)").children("input").val();
				var max = $(data).children("td:nth(11)").children("input").val();
				var dp = Number($(data).children("td:nth(6)").children("input").val());
				
				
				if(obj.attrTy==2){
					if(min.lastIndexOf(".")==-1){
						alert("소수점 자리가 맞지 않습니다.");
						$(data).children("td:nth(10)").children("input").focus();
						valid = false;
						return;
					};
					
					var min_dp_length = min.substr(min.lastIndexOf(".")+1).length;
					if(dp!=min_dp_length){
						alert("소수점 자리가 맞지 않습니다.");
						$(data).children("td:nth(10)").children("input").focus();
						valid = false;
						return;
					}
					
					if(max.lastIndexOf(".")==-1){
						alert("소수점 자리가 맞지 않습니다.");
						$(data).children("td:nth(11)").children("input").focus();
						valid = false;
						return;
					};
					
					var max_dp_length = max.substr(max.lastIndexOf(".")+1).length;
					if(dp!=max_dp_length){
						alert("소수점 자리가 맞지 않습니다.");
						$(data).children("td:nth(11)").children("input").focus();
						valid = false;
						return;
					};
					
					if(target.lastIndexOf(".")==-1){
						alert("소수점 자리가 맞지 않습니다.");
						$(data).children("td:nth(9)").children("input").focus();
						valid = false;
						return;
					};
					
					var target_dp_length = target.substr(target.lastIndexOf(".")+1).length;
					if(dp!=target_dp_length){
						alert("소수점 자리가 맞지 않습니다.");
						$(data).children("td:nth(9)").children("input").focus();
						valid = false;
						return;
					};
					
					if(min==""){
						alert("하한 값이 비어있습니다.");
						$(data).children("td:nth(10)").children("input").focus();
						valid = false;
						return;
					};
					
					if(max==""){
						alert("상한 값이 비어있습니다.");
						$(data).children("td:nth(11)").children("input").focus();
						valid = false;
						return;
					};
					
					if(target==""){
						alert("목표 값이 비어있습니다.");
						$(data).children("td:nth(9)").children("input").focus();
						valid = false;
						return;
					};
					
					if(Number(target) > Number(max)){
						alert("목표 값이 상한 값보다 큽니다.");
						$(data).children("td:nth(9)").children("input").focus();
						valid = false;
						return;
					}else if(Number(target) < Number(min)){
						alert("목표 값이 하한 값보다 작습니다.");
						$(data).children("td:nth(9)").children("input").focus();
						valid = false;
						return;
					}else if(Number(max) < Number(min)){
						alert("상한 값이 하한 값보다 작습니다.");
						$(data).children("td:nth(11)").children("input").focus();
						valid = false;
						return;
					}	
					return;
				}			
			}
		});
		
		if(!valid) return;
		
		var obj = new Object();
		obj.val = valueArray;
		
		var url = "${ctxPath}/chart/addCheckStandard.do";
		var param = "val=" + JSON.stringify(obj);
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType :"text",
			success : function(data){
				if(data=="success") {
					alert ("${save_ok}");
					getChkStandardList();
				}
			}
		}); 
	};

	var row;
	function changeAttrTy(el){
		//1 정성검사 
		//2 정량검사	
		var ty = el.value;
		$(el).parent("td").parent("tr").children("td").children("input").attr("disabled", false).css("background-color", "#ffffff");
		
		if(ty==1){
			$(el).parent("td").parent("tr").children("td:nth(4)").children("font").html("L")
			$(el).parent("td").parent("tr").children("td:nth(6)").children("input").val("").attr("disabled", true).css("background-color", "#929292");
			$(el).parent("td").parent("tr").children("td:nth(7)").children("input").val("").attr("disabled", true).css("background-color", "#929292");
			$(el).parent("td").parent("tr").children("td:nth(9)").children("input").val("").attr("disabled", true).css("background-color", "#929292");
			$(el).parent("td").parent("tr").children("td:nth(10)").children("input").val("").attr("disabled", true).css("background-color", "#929292");
			$(el).parent("td").parent("tr").children("td:nth(11)").children("input").val("").attr("disabled", true).css("background-color", "#929292");
			$(el).parent("td").parent("tr").children("td:nth(13)").children("select").attr("disabled", false).css("background-color", "#ffffff");
		}else{
			$(el).parent("td").parent("tr").children("td:nth(4)").children("font").html("N")
			$(el).parent("td").parent("tr").children("td:nth(13)").children("select").attr("disabled", true).css("background-color", "#929292");
		}
	};

	function delRow(el){
		$(el).parent("td").parent("tr").remove();
	};

	var chkTy;

	var attrTy = "<select onchange='changeAttrTy(this)' style='font-size : " + getElSize(40) + "'>" + 
					"<option value='1'>정성검사</option>" +
					"<option value='2'>정량검사</option>" + 
				"</select>";

	var JSGroupCode;

	function getGroupCode(){
		var url = "${ctxPath}/chart/getJSGroupCode.do";
		
		var options = "";
		
		$.ajax({
			url : url,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				
				console.log(data.dataList)
				$(data.dataList).each(function(idx,data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				JSGroupCode = "<select>" + options + "</select>";
			}
		});
		
		return options;
	};

	function addRow(){
		var row = "<tr class='ContentTr' id='tr0'>" + 
					"<td>" + $("#prdNo").val() + "</td>" +
					"<td>" + $("#dvcId option:selected").html() + "</td>" +
					"<td>" + chkTy + "</td>" + //검사유형
					"<td>" + attrTy + "</td>" + //특성유형 
					"<td><font>L</font><input type='text' size='6'></td>" + //특성코드
					"<td>" + "<input type='text'>" + "</td>" + //특성명(한글)
					"<td>" + "<input type='text' size='2' disabled='disable' style='background-color:#929292'> "+"</td>" + //소수점 
					"<td>" + "<input type='text' size='2' disabled='disable' style='background-color:#929292'> "+"</td>" +  //단위 
					"<td>" + "<input type='text'>" + "</td>" + //도면 
					"<td>" + "<input type='text' size='6' disabled='disable' style='background-color:#929292'> "+"</td>" + //목표 값 
					"<td>" + "<input type='text' size='6' disabled='disable' style='background-color:#929292'> "+"</td>" + //하한 값
					"<td>" + "<input type='text' size='6' disabled='disable' style='background-color:#929292'> "+"</td>" + //상한 값
					"<td>" + "<input type='text' size='8'> "+"</td>" + //계측기 
					"<td>" + JSGroupCode + "</td>" + //정성그룹코드
					"<td>" + "<input type='text'>" + "</td>" + //특성명(기타언어)
					"<td><button onclick='chkDel(this)'>삭제</button></td>" +
				"</tr>";
				
		$("#table2").append(row);
		
		$("#table2 td").css({
			"font-size" :getElSize(40),
			"padding" : getElSize(20)		
		});
	};

	var delId;
	var delEl;

	function chkDel(obj){
		$("#delDiv").css("z-index",9);
		
		delId = $(obj).parent("td").parent("tr").attr("id").substr(2);
		delEl = $(obj);
	};

	function noDel(){
		$("#delDiv").css("z-index",-1);
	};

	function okDel(){
		if(delId==0){
			delRow(delEl);
			noDel();
			return;
		}
		
		var url = ctxPath + "/chart/delChkStandard.do";
		var param = "id=" + delId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					noDel();
					delRow(delEl);
				}
			}
		}); 
	};
	</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="bannerDiv">
	<Center>
		<input type="text" id="banner" size="100"><input type="color" id="color"><br>
		<button onclick="addBanner();">적용</button>
		<button onclick="cancelBanner();">미적용</button>
	</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/quality_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<table style="width: 100%" id="content_table2">
						<Tr>
							<td style="text-align: center;"> 
								<spring:message code="prd_no"></spring:message>
								<select id="prdNo"></select>
								<spring:message code="device"></spring:message> 
								<select id="dvcId"></select>
								<spring:message code="check_type"></spring:message>
								<select  id="chkTy"></select>
								<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getChkStandardList()">
							</td>
						</Tr>		
						<tr>
							<Td style="text-align: right;">
								<button  onclick="addRow()"><spring:message code="add"></spring:message></button>
								<button  onclick="saveRow()"><spring:message code="save"></spring:message></button>
							</Td>
						</tr>
					</table>
					<div id="contentDiv">
						<table id="content_table" style="width: 100%"> 
							<tr>
								<td>
									<div id="wrapper">
										<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" id="table2" border="1" >
											<thead>
												<tr style="background-color: #222222">
													<Td><spring:message code="prd_no"></spring:message></Td>
													<Td><spring:message code="device"></spring:message></Td>
													<Td><spring:message code="check_type"></spring:message></Td>
													<Td><spring:message code="character_type"></spring:message></Td>
													<Td><spring:message code="character_cd"></spring:message></Td>
													<Td><spring:message code="character_name"></spring:message></Td>
													<Td style="white-space: nowrap;"><spring:message code="dp"></spring:message></Td>
													<Td><spring:message code="unit"></spring:message></Td>
													<Td><spring:message code="drawing"></spring:message></Td>
													<Td><spring:message code="target_val"></spring:message></Td>
													<Td><spring:message code="min_val"></spring:message></Td>
													<Td><spring:message code="max_val"></spring:message></Td>
													<Td><spring:message code="measurer"></spring:message></Td>
													<Td><spring:message code="js_group_cd"></spring:message></Td>
													<Td style="width: 10%"><spring:message code="character_name"></spring:message></Td>
													<Td><spring:message code="del"></spring:message></Td>
												</tr>
											</thead>
										</table>
									</div>
								</td>
							</tr>			
						</table> 
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	