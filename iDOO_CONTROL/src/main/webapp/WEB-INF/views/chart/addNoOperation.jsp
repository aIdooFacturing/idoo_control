<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function getBanner(){
		var url = "${ctxPath}/chart/getBanner.do";
		var param = "shopId=" + shopId;
		

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				if(window.localStorage.getItem("banner")=="true"){
					$("#intro").html(data.msg).css({
						"color" : data.rgb,
						"right" : - window.innerWidth
					})
					
					$("#intro").html(data.msg).css({
						"right" : - $("#intro").width()
					})
				}
				
				bannerWidth = $("#intro").width() + getElSize(100); 
				$("#intro").width(bannerWidth);
				$("#banner").val(data.msg);
				$("#color").val(data.rgb);
				
				twinkle();
				bannerAnim();
			}
		});		
	}
	
	var twinkle_opct = false;
	function twinkle(){
		var opacity;
		if(twinkle_opct){
			opacity = 0;
		}else{
			opacity = 1;
		}
		$("#intro").css("opacity",opacity);
		
		twinkle_opct = !twinkle_opct;
		setTimeout(twinkle, 300)
	};
	
	var bannerWidth;
	function bannerAnim(){
		$("#intro").width(bannerWidth - getElSize(10));
		$("#intro").animate({
			"right" : window.innerWidth  - getElSize(100)
		},8000, function(){
			$("#intro").css("right" , - $("#intro").width())
			//$(this).remove();
			bannerAnim();
		});
	};
	
	function chkBanner(){
		if(window.localStorage.getItem("banner")=="true"){
			getBanner();
			$("#intro_back").css("display","block");
		}else{
			$("#intro").html("");
			$("#intro_back").css("display","none");
		}
	};
	
	function cancelBanner(){
		window.localStorage.setItem("banner", "false");
		$("#bannerDiv").css("z-index",-9);
		chkBanner();
	};
	
	function addBanner(){
		var url = "${ctxPath}/chart/addBanner.do";
		var param = "msg=" + encodeURIComponent($("#banner").val()) + 
					"&rgb=" + $("#color").val() + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				window.localStorage.setItem("banner", "true");
				location.reload();
				/* $("#bannerDiv").css("z-index",-9);
				chkBanner(); */
			}
		});
	};
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	function caldate(day){
		 var caledmonth, caledday, caledYear;
		 var loadDt = new Date();
		 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
		 
		 caledYear = v.getFullYear();
		 
		 if( v.getMonth() < 9 ){
		  caledmonth = '0'+(v.getMonth()+1);
		 }else{
		  caledmonth = v.getMonth()+1;
		 }
		 if( v.getDate() < 9 ){
		  caledday = '0'+v.getDate();
		 }else{
		  caledday = v.getDate();
		 }
		 return caledYear + "-" + caledmonth+ '-' + caledday;
		}
		
	
	var handle = 0;
	var className = "";
	var classFlag = true;
	
	function getWorkerList(){
		var url = "${ctxPath}/chart/getWorkerList.do";
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var option = "";
				if(chkLang!="ko"){
					option = "<option></option>";
				}
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				$("#worker_form").html(option);
			}
		});
	};
	
	var className = "";
	var classFlag = true;

	function getNonOpTy(){
		var url = ctxPath + "/chart/getNonOpTy.do"
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decodeURIComponent(data.nonOpTy).replace(/\+/gi, " ") + "</option>";
				});
				
				$("#nonOpTy_form").html(options);
			}
		});		
	};
	
	function getPrdNo(){
		var url = "${ctxPath}/chart/getPrdNoList.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>";
				});
				
				$("#group").html(option);
				$("#prdNo_form").html(option).change(getDvcList);
				
				//getNonOpInfo();
				getOprNm();
				getDvcList();
			}
		});
	};
	
	$(function(){
		createNav("order_nav", 3);
		getWorkerList();
		getNonOpTy();
		bindEvt2();
		getPrdNo();
		$(".excel").click(csvSend);
		setEl();
		setDate();
		
		time();
		
		$("#home").click(function(){ location.href = "/iDOO_CONTROL/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		showInsertForm();
	});
	
	function calcTimeDiff(start, end){
		var startHour = Number(start.substr(0,2));
		var startMinute = Number(start.substr(3,2));
		var endHour = Number(end.substr(0,2));
		var endMinute = Number(end.substr(3,2));
		
		var startTime = new Date($("#startDate_form").val() + "," + $("#startTime_form").val());
		var endTime = new Date($("#endDate_form").val() + "," + $("#endTime_form").val());
		
		var timeDiff = (endTime.getTime() - startTime.getTime()) / 1000 /60;
		
		return timeDiff;
	}
	
	function showInsertForm(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day= addZero(String(date.getDate()));
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		
		$("input[type='time']").val(hour + ":" + minute).change(function(){
			$("#time_form").html(calcTimeDiff($("#startTime_form").val(), $("#endTime_form").val()))	
		});
		
		$(".date").val(year + "-" + month + "-" + day).change(function(){
			$("#time_form").html(calcTimeDiff($("#startTime_form").val(), $("#endTime_form").val()))	
		});
		
		//showCorver();
		$("#insertForm").css("z-index",999);
		
		return false;
	};
	
	function updateNonOp(){
		var url = "${ctxPath}/chart/updateNonOp.do";
		var param = "id=" + nonId + 
					"&prdNo=" + $("#update #prdNo").val() + 
					"&oprNm=" + $("#update #oprNm").val() +
					"&dvcId=" + $("#update #dvcId").val() +
					"&date=" + $("#update #date").val() +
					"&ty=" + $("#update #ty").val() +
					"&worker=" + $("#update #worker").val() +
					"&nonOpTy=" + $("#update #nonOpTy").val() +
					"&startTime=" + $("#update #startTime").val() +
					"&endTime=" + $("#update #endTime").val() + 
					"&time=" + calcTimeDiff($("#update #startTime").val(), $("#update #endTime").val());
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success :function(data){
				if(data=="success"){
					closeInsertForm();
					//getNonOpInfo();
				}
			}
		});
		
		return false;
	};
	
	function addNonOp(){
		var url = "${ctxPath}/chart/addNonOp.do";
		var param = "prdNo=" + $("#prdNo_form").val() + 
					"&oprNm=" + $("#oprNm_form").val() + 
					"&dvcId=" + $("#dvcId_form").val() + 
					"&date=" + $("#date_form").val() + 
					"&ty=" + $("#workIdx").val() +
					"&worker=" + $("#worker_form").val() +
					"&nonOpTy=" + $("#nonOpTy_form").val() +
					"&startTime=" + $("#startTime_form").val() + 
					"&endTime=" + $("#endTime_form").val() + 
					"&time=" + calcTimeDiff($("#startTime_form").val(), $("#endTime_form").val());
		
		console.log(param)
		if($("#dvcId_form").val()==0){
			alert("장비를 선택하세요.");
			$("#dvcId_form").focus();
			
			return false;
		}else if($("#nonOpTy_form").val()==0){
			alert("비가동 유형을 선택하세요.");
			$("#nonOpTy_form").focus();
			
			return false;
		}else if($("#worker_form").val()==0){
			alert("작업자를 입력하세요.");
			$("#worker_form").focus();
			
			return false;
		}
		
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					/* closeInsertForm();
					setEl();
					getNonOpInfo(); */
					alert("저장 되었습니다.")
				}	
			}
		});
		
		return false;
	};
	
	function closeInsertForm(){
		hideCorver();
		$("#insertForm, #updateForm").css("z-index",-9999);
		return false;
	}
	
	function bindEvt2(){
		$("#addBtn").click(showInsertForm);
		$("#modify").click(updateNonOp);
		
		$("#insert #cancel, #update #cancel").click(closeInsertForm);
		$("#insert #lotCnt").keyup(calcAQL);
		$("#insert #notiCnt").keyup(calcAQL);
		$("#update #lotCnt").keyup(calcAQL_update);
		$("#update #notiCnt").keyup(calcAQL_update);
	};
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function calcAQL(){
		var lotCnt = $("#insert #lotCnt").val();
		var notiCnt = $("#insert #notiCnt").val();
		
		$("#insert #smplCnt").html("5");
		if(notiCnt>0){
			$("#insert #rcvCnt").html("0").css("color","red");	
		}else{
			$("#insert #rcvCnt").html(lotCnt).css("color","white");
		}
	};

	function calcAQL_update(){
		var lotCnt = $("#update #lotCnt").val();
		var notiCnt = $("#update #notiCnt").val();
		
		$("#update #smplCnt").html("5");
		if(notiCnt>0){
			$("#update #rcvCnt").html("0").css("color","red");	
		}else{
			$("#update #rcvCnt").html(lotCnt).css("color","white");
		}
	};
	
	function getDvcList(){
		var url = ctxPath + "/chart/getDevieList.do"
		var param = "shopId=" + shopId + 
					"&prdNo=" + $("#prdNo_form").val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
				});
				
				$("#dvcId_form").html(options);
			}
		});	
	};

	function getNonOpInfo(){
		classFlag = true;
		var url = "${ctxPath}/chart/getNonOpInfo.do";
		var sDate = $("#sDate").val();
		
		var param = "sDate=" + sDate +
					"&prdNo=" + $("#group").val() 
					
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				$("#tbody").empty();
				
				var tr;
							
				$(json).each(function(idx, data){
					if(data.seq!="."){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						
						var nonTy;
						if(data.ty==1){
							nonTy = "야간";
						}else{
							nonTy = "주간"
						}
						
						tr += "<tr class='contentTr " + className + "' id='tr" + data.id + "'>" +
									"<td >" + data.prdNo + "</td>" +
									"<td>" + decodeURIComponent(data.oprNm).replace(/\+/gi, " ") + "</td>" + 
									"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" +
									"<td>" + data.date + "</td>" + 
									"<td>" + nonTy + "</td>" + 
									"<td>" + Math.round(data.cuttingTime/60) + "</td>" + 
									"<td>" + Math.round(data.inCycleTime/60) + "</td>" + 
									"<td>" + Math.round(data.waitTime/60) + "</td>" + 
									"<td>" + Math.round(data.alarmTime/60) + "</td>" + 
									"<td>" + Math.round(data.noConnectionTime/60) + "</td>" +
									"<td>" + data.opRatio + "%</td>" +
									"<td>" + data.cuttingTimeRatio + "%</td>" +
									"<td>" + decodeURIComponent(data.worker).replace(/\+/gi, " ") + "</td>" +
									"<td>" + data.totalTime + "</td>" +
									"<td>" + data.nonOpTime + "</td>" +
									"<td>" + (data.totalTime - data.nonOpTime) + "</td>" +
									"<td>" + Math.round((data.totalTime - data.nonOpTime) / data.totalTime * 100) + "%</td>" +
									"<td>" + decodeURIComponent(data.nonOpTyTxt).replace(/\+/gi, " ") + "</td>" +
									"<td>" + data.startTime + "</td>" +
									"<td>" + data.endTime + "</td>" +
									"<td>" + calcTimeDiff(data.startTime, data.endTime) + "</td>" +
									"<td><button onclick='showUpdateForm(" + data.id + ")'>수정</button></td>" +
									"<td><button onclick='chkDel(\"" + data.id + "\")'>삭제</button></td>" +
							"</tr>";					
					}
				});
				
				
				$("#tbody").append(tr);
				
				$(".alarmTable").css({
					"font-size": getElSize(40),
				});
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid black"
				});
				
				$("#wrapper").css({
					"height" :getElSize(1650),
					"width" : "100%",
					"overflow" : "hidden"
				});
				
				$("#wrapper").css({
					"margin-left" : (contentWidth/2) -($("#wrapper").width()/2)
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("#wrapper div:last").remove();
				scrolify($('#table'), getElSize(1450));
				$("#wrapper div:last").css("overflow", "auto")
				
				
			}
		});
	};
	
	function getOprNm(prdNo){
		var url = "${ctxPath}/chart/getOprNoList.do";

		$.ajax({
			url : url,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
		
				var option = "";
				
				if(chkLang!="ko"){
					option = "<option></option>";
				}
				
				$(json).each(function(idx, data){
					if(data.oprNm=="0010"){
						oprNo = "R삭";
					}else if(data.oprNm=="0020"){
						oprNo = "MCT 1차";
					}else if(data.oprNm=="0030"){
						oprNo = "CNC 2차";
					}
					
					option += "<option value='" + data.oprNm + "'>" + oprNo + "</option>";
				});
				
				$("#oprNm_form").html(option)
				
				
			}
		});
		/* var url = "${ctxPath}/chart/getOprNm.do";
		var param = "prdNo=" + prdNo;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
		
				var option = "";
				
				$(json).each(function(idx, data){
					option += "<option value='" + decodeURIComponent(data.oprNm).replace(/\+/gi, " ") + "'>" + decodeURIComponent(data.oprNm).replace(/\+/gi, " ") + "</option>";
				});
				
				$("#oprNm_form").html(option)
			}
		}); */
	};

	var csvOutput;
	function csvSend(){
		var sDate, eDate;
		
		sDate = $("#sDate").val();
		eDate = $("#eDate").val();
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = eDate;
		f.submit(); 
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"display" : "none",
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

	
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(35),
			"padding" : getElSize(20),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(50),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(40),
			"padding" : getElSize(20),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		
		//
		$("#insertForm").css({
			"width" : getElSize(3000),
			"position" : "absolute",
			"z-index" : -999,
		});
		
		$("#insertForm").css({
			"left" : $(".menu_right").offset().left + ($(".menu_right").width()/2) - ($("#insertForm").width()/2) - marginWidth,
			"top" : getElSize(400)
		});
		
		$("#insert table td").css({
			"color" : "white",
			"font-size" : getElSize(50),
			"padding" : getElSize(20),
			"text-align" : "Center"
		});
		
		$("#insertForm table td").css({
			"font-size" : getElSize(80),
			"padding" : getElSize(30),
			"background-color" : "#323232"
		});
		
		$("#insertForm button, #insertForm select, #insertForm input").css({
			"font-size" : getElSize(70),
			"margin" : getElSize(20)
		});
		
		$(".table_title").css({
			"background-color" : "#222222",
			"color" : "white"
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function goGraph(){
		location.href = "${ctxPath}/chart/jigGraph.do"
	};
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="bannerDiv">
	<Center>
		<input type="text" id="banner" size="100"><input type="color" id="color"><br>
		<button onclick="addBanner();">적용</button>
		<button onclick="cancelBanner();">미적용</button>
	</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/order_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<tr>
							<td>
								<div id="insertForm">
									<table style="width: 100%">
										<Tr> 
											<Td class='table_title' width="20%"> <spring:message code="prdct_machine_line"></spring:message></Td> <Td width="30%"><select id="prdNo_form"></select> </Td> <Td class='table_title'  width="20%">  <spring:message code="operation"></spring:message> </Td> <Td  ><select id="oprNm_form"></select> </Td>
										</Tr>
										<Tr>
											<Td class='table_title'><spring:message code="machine_order"></spring:message>  </Td> <Td ><select id="dvcId_form"></select> </Td> <Td class='table_title'> <spring:message code="date_"></spring:message> </Td> <Td><input type="date" id="date_form" class="date"> </Td>
										</Tr>
										<Tr> 
											<Td class='table_title'> <spring:message code="division"></spring:message> </Td> <Td ><select id="workIdx"><option value="2"><spring:message code="day"></spring:message></option><option value="1"><spring:message code="night"></spring:message></option> </select> </Td> <Td class='table_title'> <spring:message code="worker"></spring:message> </Td> <Td><select id="worker_form"></select> </Td>
										</Tr>
										<Tr> 
											<Td class='table_title'> <spring:message code="start"></spring:message>	 </Td> <Td colspan="3"> <input type="date" id="startDate_form" class="date"> <input type="time" id="startTime_form"> </Td> 
										</Tr>
										<Tr> 
											<Td class='table_title'>  <spring:message code="end"></spring:message>	 </Td> <Td colspan="3"> <input type="date" id="endDate_form" class="date"> <input type="time" id="endTime_form"> </Td> 
										</Tr>
										<Tr> 
											<Td class='table_title'> <spring:message code="non_operation_ty"></spring:message> <Td><select id="nonOpTy_form"></select> </Td> <Td class='table_title' ><spring:message code="time_minute"></spring:message> </Td> <Td id="time_form" style="color: white"></Td>
										</Tr>
										<Tr>
											<Td colspan="4" style="text-align: center;"><button onclick="addNonOp()"> <spring:message code="save"></spring:message> </button></Td>
										</Tr>
									</table> 
								</div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	