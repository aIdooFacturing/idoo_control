<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function getBanner(){
		var url = "${ctxPath}/chart/getBanner.do";
		var param = "shopId=" + shopId;
		

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				if(window.localStorage.getItem("banner")=="true"){
					$("#intro").html(data.msg).css({
						"color" : data.rgb,
						"right" : - window.innerWidth
					})
					
					$("#intro").html(data.msg).css({
						"right" : - $("#intro").width()
					})
				}
				
				bannerWidth = $("#intro").width() + getElSize(100); 
				$("#intro").width(bannerWidth);
				$("#banner").val(data.msg);
				$("#color").val(data.rgb);
				
				twinkle();
				bannerAnim();
			}
		});		
	}
	
	var twinkle_opct = false;
	function twinkle(){
		var opacity;
		if(twinkle_opct){
			opacity = 0;
		}else{
			opacity = 1;
		}
		$("#intro").css("opacity",opacity);
		
		twinkle_opct = !twinkle_opct;
		setTimeout(twinkle, 300)
	};
	
	var bannerWidth;
	function bannerAnim(){
		$("#intro").width(bannerWidth - getElSize(10));
		$("#intro").animate({
			"right" : window.innerWidth  - getElSize(100)
		},8000, function(){
			$("#intro").css("right" , - $("#intro").width())
			//$(this).remove();
			bannerAnim();
		});
	};
	
	function chkBanner(){
		if(window.localStorage.getItem("banner")=="true"){
			getBanner();
			$("#intro_back").css("display","block");
		}else{
			$("#intro").html("");
			$("#intro_back").css("display","none");
		}
	};
	
	function cancelBanner(){
		window.localStorage.setItem("banner", "false");
		$("#bannerDiv").css("z-index",-9);
		chkBanner();
	};
	
	function addBanner(){
		var url = "${ctxPath}/chart/addBanner.do";
		var param = "msg=" + encodeURIComponent($("#banner").val()) + 
					"&rgb=" + $("#color").val() + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				window.localStorage.setItem("banner", "true");
				location.reload();
				/* $("#bannerDiv").css("z-index",-9);
				chkBanner(); */
			}
		});
	};
	
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(el, val){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		el.val(year + "-" + month + "-" + day);
		if(typeof(val)!="undefined"){
			el.val(val);	
		}
	};
	
	var handle = 0;
	
	$(function(){
		createNav("quality_nav", 2);
		
		setEl();
		time();
		
		$("#home").click(function(){ location.href = "/iDOO_CONTROL/chart/index.do"; });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		addRow();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"display" : "none",
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

	
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#contentDiv").css({
			"overflow" : "auto",
			"width" : $(".menu_right").width()
		});
		
		$("#insertForm").css({
			"width" : getElSize(3000),
			"position" : "absolute",
			"z-index" : 999,
		});
		
		$("#insertForm").css({
			"left" : $(".menu_right").offset().left + ($(".menu_right").width()/2) - ($("#insertForm").width()/2) - marginWidth,
			"top" : getElSize(400)
		});
		
		$("#insertForm table td").css({
			"font-size" : getElSize(70),
			"padding" : getElSize(15),
			"background-color" : "#323232"
		});
		
		$("#insertForm button, #insertForm select, #insertForm input").css({
			"font-size" : getElSize(60),
			"margin" : getElSize(15)
		});
		
		$(".table_title").css({
			"background-color" : "#222222",
			"color" : "white"
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	

	var valArray = [];

	function saveRow2(){
		valArray = [];
		
		var obj = new Object();
		
		obj.chkTy = $("#chkTy_form select").val();
		obj.prdNo = $("#prdNo_form select").val();
		obj.prdPrc = $("#oprNm_form select").val();
		obj.dvcId = $("#dvcId_form select").val();
		obj.date = $("#date_form input").val();
		obj.checker = $("#checker_form input").val();
		obj.part = $("#part_form select").val();
		obj.situationTy = $("#situationTy_form select").val();
		obj.situation = $("#situation_form select").val();
		obj.cause = $("#cause_form select").val();
		obj.gchTY = $("#gchTy_form select").val();
		obj.gch = $("#gch_form select").val();
		obj.cnt = $("#cnt_form input").val();
		obj.action = $("#action_form select").val();
		
		valArray.push(obj);
		
		var obj = new Object();
		obj.val = valArray;
		
		var url = "${ctxPath}/chart/saveRow.do";
		var param = "val=" + JSON.stringify(obj);
		
		if($("#prdNo_selector_form").val()=="0"){
			alert("품번을 선택하십시오.");
			$("#prdNo_selector_form").focus();
			return;
		}else if($("#dvcId_form select").val()=="0"){
			alert("장비를 선택하십시오.");
			$("#dvcId_form select").focus();
			return;
		};
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					alert("${save_ok}");
				}
			}
		});	
	};
	
	function addRow(){
		if(classFlag){
			className = "row2"
		}else{
			className = "row1"
		};
		classFlag = !classFlag;
		
		var checkTy = document.createElement("select");
		var prdNo = document.createElement("select");
		prdNo.setAttribute("id", "prdNo_selector_form");
		var process = document.createElement("select");
		var device = document.createElement("select");
		var sDate = document.createElement("input");
		sDate.setAttribute("type", "date");
		var text = document.createElement("input");
		text.style.cssText = "width : " + getElSize(400);
		var cnt = document.createElement("input");
		cnt.setAttribute("class", "cnt");
		cnt.setAttribute("size", 5);
		var part = document.createElement("select");
		var situationTy = document.createElement("select");
		var situation = document.createElement("select");
		var cause = document.createElement("select");
		var gchTY = document.createElement("select");
		var gch = document.createElement("select");
		var action = document.createElement("select");
		
		var tr = document.createElement("tr");
		tr.setAttribute("class", className);
		
		var checkTy_td = document.createElement("td");
		checkTy_td.append(checkTy);
		tr.append(checkTy_td);
		$("#chkTy_form").html(checkTy_td)
		
		var prdNo_td = document.createElement("td");
		prdNo_td.append(prdNo);
		tr.append(prdNo_td);
		$("#prdNo_form").html(prdNo_td)
		
		var process_td = document.createElement("td");
		process_td.append(process);
		tr.append(process_td);
		$("#oprNm_form").html(process_td)
		
		var device_td = document.createElement("td");
		device_td.append(device);
		tr.append(device_td);
		$("#dvcId_form").html(device_td)
		
		var sDate_td = document.createElement("td");
		sDate_td.append(sDate);
		tr.append(sDate_td);
		$("#date_form").html(sDate_td)
		
		var text_td = document.createElement("td");
		text_td.append(text);
		tr.append(text_td);
		$("#checker_form").html(text_td)
		
		var part_td = document.createElement("td");
		part_td.append(part);
		tr.append(part_td);
		$("#part_form").html(part_td)
		
		var situ_td = document.createElement("td");
		situ_td.append(situationTy);
		tr.append(situ_td);
		$("#situation_form").html(situ_td)
		
		var situTy_td = document.createElement("td");
		situTy_td.append(situation);
		tr.append(situTy_td);
		$("#situationTy_form").html(situTy_td)
		
		var cause_td = document.createElement("td");
		cause_td.append(cause);
		tr.append(cause_td);
		$("#cause_form").html(cause_td)
		
		var gchkTy_td = document.createElement("td");
		gchkTy_td.append(gchTY);
		tr.append(gchkTy_td);
		$("#gchTy_form").html(gchkTy_td)
		
		var gch_td = document.createElement("td");
		gch_td.append(gch);
		tr.append(gch_td);
		$("#gch_form").html(gch_td)
		
		var cnt_td = document.createElement("td");
		cnt_td.append(cnt);
		tr.append(cnt_td);
		$("#cnt_form").html(cnt_td)
		
		var action_td = document.createElement("td");
		action_td.append(action);
		tr.append(action_td); 
		$("#action_form").html(action_td)
		
		var button_td = document.createElement("td");
		var button = document.createElement("button");
		//button.setAttribute("id", "b" + data.id);
		var button_text = document.createTextNode("삭제");
		button.append(button_text);
		button_td.append(button)
		tr.append(button_td);
		
		//$("#tbody").append(tr);
		
		getCheckTyList($(checkTy));
		
		getProcessList($(process));
		getDevieList($(device));
		setDate($(sDate));
		getPartList($(part));
		getSituationList($(situation));
		getSituationTyList($(situationTy));
		getCauseList($(cause));
		getGChTyList($(gchTY));
		getGChList(7, $(gch));
		getActionList($(action)); 
		getPrdNoList($(prdNo));
		$(cnt).val(1);
		
		$(".row1").not(".tr_table_fix_header").css({
			"background-color" : "#222222"
		});

		$(".row2").not(".tr_table_fix_header").css({
			"background-color": "#323232"
		});
		
		
		$("#insertForm select, #insertForm input").css("font-size",getElSize(80))
		
		
		showCorver();
		$("#insertForm").css("z-index", 999)
		
		if(addFaulty!=""){
			$("#prdNo_selector_form option[value='" + $prdNo + "']").attr("selected", "selected");
			$("#cnt_form").children("td").children("input").val($cnt);
		}
		
		setEl();
	};
	
	function getGch(){
		var val = $(this).val();
		var ty;
		if(val=="47"){			//업체
			ty = "com";
		}else if(val=="48"){	//작업자  
			ty = "worker"	
		};
		
		getGChList(ty, $("#gch_form").children("td").children("select"));
	};
	
	var className = "";
	var classFlag = true;

	var menu = false;

	function getProcessList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 3; 	
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getPartList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 4;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getSituationTyList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 5;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getCnt(el, id){
		var url = ctxPath + "/chart/getCnt.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				el.val(data);
			}
		});	
	};

	function getChecker(el, id){
		var url = ctxPath + "/chart/getChecker.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				el.val(decode(data));
			}
		});	
	};

	function getSituationList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 6;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getCauseList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 7;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getGChTyList(el,val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 8;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options).change(getGch);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getGChList(ty, el){
		//var url = ctxPath + "/chart/getCheckTyList.do"
		var url;
		if(ty=="com"){
			url = ctxPath + "/chart/getComList.do"		
		}else if(ty=="worker"){
			url = ctxPath + "/chart/getWorkerList.do"
		}else{
			el.html("<option value='0'>${selection}</option>");
			return; 
		}
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
					
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getActionList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 10;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getCheckTyList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 2;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getPrdNoList(el, val){
		var url = ctxPath + "/chart/getPrdNoList.do"
		
		$.ajax({
			url : url,
			dataType : "json",
			async : false,
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>";
				});
				
				el.html(options).change(function() {getDevieList($("#dvcId_form").children("td").children("select"), this)});
				
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});
	};

	function getGChTyList(el,val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 8;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options).change(getGch);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};
	
	var addFaulty = "${addFaulty}";
	function getDevieList(el, obj){
		var url = ctxPath + "/chart/getDevieList.do"
		var param = "shopId=" + shopId + 
					"&prdNo=" + $(obj).val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
			}
		});	
	};
	</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="bannerDiv">
	<Center>
		<input type="text" id="banner" size="100"><input type="color" id="color"><br>
		<button onclick="addBanner();">적용</button>
		<button onclick="cancelBanner();">미적용</button>
	</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/quality_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<div id="insertForm">
						<table style="width: 100%">
							<Tr> 
								<Td class='table_title' width="20%"> <spring:message code="check_ty"></spring:message> </Td> <Td id="chkTy_form"  width="30%"></Td> <Td class='table_title'  width="20%"><spring:message code="operation"></spring:message> </Td> <Td id="oprNm_form"></Td> 
							</Tr>
							<Tr> 
								<Td class='table_title'> <spring:message code="prd_no"></spring:message>  </Td> <Td id="prdNo_form"> </Td><Td class='table_title'> <spring:message code="device"></spring:message> </Td> <Td id="dvcId_form"></Td>
							</Tr>
							<Tr> 
								<Td class='table_title'> <spring:message code="event_date"></spring:message> </Td> <Td id="date_form"></Td> <Td class='table_title'> <spring:message code="reporter"></spring:message> </Td> <Td id="checker_form"></Td>
							</Tr>
							<Tr> 
								<Td class='table_title'> <spring:message code="part"></spring:message> </Td> <Td id="part_form"></Td> <Td class='table_title'> <spring:message code="divide_situ"></spring:message> </Td> <Td id="situationTy_form"></Td>
							</Tr>
							<Tr> 
								<Td class='table_title'> <spring:message code="situ"></spring:message> </Td> <Td id="situation_form"></Td> <Td class='table_title' > <spring:message code="situ"></spring:message> </Td> <Td id="cause_form"></Td>
							</Tr>
							<Tr> 
								<Td class='table_title'>  <spring:message code="gch_ty"></spring:message>  </Td> <Td id="gchTy_form"></Td> <Td class='table_title'> <spring:message code="gch"></spring:message> </Td> <Td id="gch_form"></Td>
							</Tr>
							<Tr> 
								<Td class='table_title'> <spring:message code="count"></spring:message> </Td> <Td id="cnt_form"></Td> <Td class='table_title'> <spring:message code="action"></spring:message>  </Td> <Td id="action_form"></Td>
							</Tr>
							<Tr>
								<Td colspan="4" style="text-align: center;"><button onclick="saveRow2()"><spring:message code="save"></spring:message> </button></Td>
							</Tr>
						</table> 
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back" style="display: none"></div>
	<span id="intro"></span>
</body>
</html>	