<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function getBanner(){
		var url = "${ctxPath}/chart/getBanner.do";
		var param = "shopId=" + shopId;
		

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				if(window.localStorage.getItem("banner")=="true"){
					$("#intro").html(data.msg).css({
						"color" : data.rgb,
						"right" : - window.innerWidth
					})
					
					$("#intro").html(data.msg).css({
						"right" : - $("#intro").width()
					})
				}
				
				bannerWidth = $("#intro").width() + getElSize(100); 
				$("#intro").width(bannerWidth);
				$("#banner").val(data.msg);
				$("#color").val(data.rgb);
				
				twinkle();
				bannerAnim();
			}
		});		
	}
	
	var twinkle_opct = false;
	function twinkle(){
		var opacity;
		if(twinkle_opct){
			opacity = 0;
		}else{
			opacity = 1;
		}
		$("#intro").css("opacity",opacity);
		
		twinkle_opct = !twinkle_opct;
		setTimeout(twinkle, 300)
	};
	
	var bannerWidth;
	function bannerAnim(){
		$("#intro").width(bannerWidth - getElSize(10));
		$("#intro").animate({
			"right" : window.innerWidth  - getElSize(100)
		},8000, function(){
			$("#intro").css("right" , - $("#intro").width())
			//$(this).remove();
			bannerAnim();
		});
	};
	
	function chkBanner(){
		if(window.localStorage.getItem("banner")=="true"){
			getBanner();
			$("#intro_back").css("display","block");
		}else{
			$("#intro").html("");
			$("#intro_back").css("display","none");
		}
	};
	
	function cancelBanner(){
		window.localStorage.setItem("banner", "false");
		$("#bannerDiv").css("z-index",-9);
		chkBanner();
	};
	
	function addBanner(){
		var url = "${ctxPath}/chart/addBanner.do";
		var param = "msg=" + encodeURIComponent($("#banner").val()) + 
					"&rgb=" + $("#color").val() + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				window.localStorage.setItem("banner", "true");
				location.reload();
				/* $("#bannerDiv").css("z-index",-9);
				chkBanner(); */
			}
		});
	};
	
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;

	function setToday(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		
		$(".date").val(year + "-" + month + "-" + day);
		$(".time").val(hour + ":" + minute);
	};

	
	$(function(){
		getDvcId();
		$("#dvcId").change(getPrgCdList);
		$("#prgCd").change(getPrgInfo);
		
		createNav("analysis_nav", 2);
		
		setEl();
		setDate();	
		time();
		
		setEvt();
		$("#home").click(function(){ location.href = "/iDOO_CONTROL/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function setEvt(){

	};
	
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"display" : "none",
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

	
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$(".tableContainer2").css({
			"margin-top" : getElSize(30)
		});
		
		$(".tableContainer").css({
			//"height" : getElSize(600),
			"height" : getElSize(500),
		});
		
		$("#legend table div").css({
			"height" : getElSize(20),
			"width" : getElSize(50),
			"overflow" : "hidden"
		});
		
		$("#legend table td").css({
			"font-size" : getElSize(40),
			"color" : "white"
		});
		
		$("#legend").css({
			"position" : "absolute",
			"z-index" : 999
		});
		
		$("#legend").css({
			"left" : $(".menu_right").offset().left - marginWidth + ($(".menu_right").width()/2) - ($("#legend").width()/2),
			"top" : getElSize(970)
		});
		
		$("#wrapper").css({
			"overflow-x" : "auto",
			"width" : $("#headerTable").width()
		});
		
		$("#lineChart").css({
			"height" : getElSize(600),
		});
		
		$("#noData").css({
			"color" : "white",
			"font-size" : getElSize(50),
			"padding-top" : getElSize(300),
			"position" : "absolute",
			"width" : $("#lineChart").width(),
			"height" : $("#lineChart").height(),
			"top" : $("#lineChart").offset().top,
			"left" : $(".menu_right").offset().left - marginWidth + ($(".menu_right").width()/2) - ($("#noData").width()/2),
			"display" : "none"
		});
		
		$("#lastDiv").css({
			//"height" : getElSize(400),
		});
		
		$("#toolInfoTable td, #opInfoTable td").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
		
		$("#opInfo, #toolInfo").css({
			"border" : getElSize(5) + "px solid white"
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function getDvcId(){
		var url = ctxPath + "/chart/getAllDvc.do";
		var param = "shopId=" + shopId + 
					"&line=ALL";
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				var option = "";
				$(json).each(function(idx, data){
					option += "<option value='" + data.dvcId + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
					
				});
				
				$("#dvcId").html(option);
				getPrgCdList();
			}
		});
		
	};

	function getPrgCdList(){
		var url = ctxPath + "/chart/getPrgCdList.do";
		var param = "dvcId=" + $("#dvcId").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				var option = "";
				$(json).each(function(idx, data){
					option += "<option value='" + data.prgCd + "'>" + decodeURIComponent(data.prgCd).replace(/\+/gi, " ") + "</option>";
					
				});
				
				$("#prgCd").html(option);
				
				getPrgInfo();
				getData();
			}
		});
	};

	var maxLength;
	var barWidth;
	function getPrgInfo(){
		var url = ctxPath + "/chart/getPrgInfo.do";
		
		var param = "dvcId=" + $("#dvcId").val() +
					"&prgCd=" + $("#prgCd").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
			
				if(json.length==0){
					$("#lineChart").css("opacity",0);
					$("#noData").css("display","inline");
					return;
				}else{
					$("#lineChart").css("opacity",1);
					$("#noData").css("display","none");
				}
				
				maxLength = json[json.length-1].sec;
				
				barWidth = getElSize(maxLength) * 114; 
				
				SLAV = [0];
				SLMX = [0];
				SLMN = [0];
				
				$(json).each(function(idx, data){
					//SLAV.push(Number(data.slAv));
					/* SLMX.push(Number(data.slMx));
					SLMN.push(Number(data.slMn)); */
				});
					
				
				var preSec = 1;
				var preSLAV, preSLMX, preSLMN;
				var src;
				
				
				$(json).each(function(idx, data){
					if(Number(data.sec) - Number(preSec) == 0){
						preSLAV = Number(data.slAv);
						preSLMX = Number(data.slMx);
						preSLMN = Number(data.slMn);
						
						SLAV.push(preSLAV);
						SLMX.push(preSLMX);
						SLMN.push(preSLMN);
						
						preSec++;
					}else{
						var loop = Number(data.sec) - (Number(preSec));
						for(var i = 0 ; i < loop; i++){
							SLAV.push(Number(preSLAV))
							SLMX.push(Number(preSLMX))
							SLMN.push(Number(preSLMN))
						}
				
						SLAV.push(Number(data.slAv))
						SLMX.push(Number(data.slMx))
						SLMN.push(Number(data.slMn))
						
						preSec = Number(data.sec)+1;
					}
				});

				getSLCR();
			}
		});
	};

	function getValue(json, idx, val){
		var rtn = "";
		$(json).each(function(idx, data){
			if(idx==$(data).sec){
				rtn = $(data).val;
				return rtn;
			}
		});
		return "";
	}
	function getSLCR(){
		var url = ctxPath + "/chart/getSLCR.do";
		var param = "dvcId=" + $("#dvcId").val() + 
					"&prgCd=" + $("#prgCd").val();
		
		$.ajax({
			url : url,
			data : param,
			type :"post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				SLCR = [];
				$(json).each(function(idx, data){
					SLCR.push(Number(data.slCr));
				});
				
				drawLineChart();
				
				var current = json.length / maxLength;
				var currentPoint = barWidth * current - getElSize(1800);
				$("#wrapper").animate({scrollLeft: currentPoint}, 100);
					
				var pointer = document.createElement("div");
				pointer.setAttribute("id", "pointer_stick");
				pointer.style.cssText = "width : " +getElSize(10) + "px;" + 
										"height :" + ($("#lineChart").height() - getElSize(130)) +"px;" + 
										"background-color : #02A3FF;" + 
										"position:absolute;" + 
										"z-index:9999;" + 
										"top : " + ($("#lineChart").offset().top + getElSize(30)) + "px;" + 
										"left:" + (window.innerWidth/2 + getElSize(10));
				
				//$("body").append(pointer)
				
			}
		});
	};

	function drawLineChart(){
		$("#lineChart").css({
			"width" : barWidth,
		});
		
		$('#lineChart').highcharts({
	        chart: {
	            type: 'spline',
	            //marginLeft :getElSize(70),
	            "width" : barWidth,
	            backgroundColor : "rgba(0,0,0,0)",
	        },
	        exporting : false,
	        credits :false,
	        title: {
	            text: false
	        },
	        subtitle: {
	            text: false
	        },
	        xAxis: {
	            categories: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],
	            title: {
	                text: false
	            }
	        },
	        yAxis: {
	            title: {
	                text: false
	            },
	            min: 0
	        },
	        legend : {
	        	verticalAlign: 'top',
	        	enabled : false,
	        	itemStyle : {
	        			color : "white",
	        			fontSize : getElSize(35)
	        		}
	        },
	        tooltip: {
	        	enabled : false,
	            headerFormat: '<b>{series.name}</b><br>',
	            pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
	        }, 

	        plotOptions: {
	            spline: {
	                marker: {
	                    enabled: false
	                }
	            }
	        },

	        series: [{
	            name: 'S.L ${avrg}',
	            lineWidth: getElSize(15),
	           	color : "#3EC373",
	            marker : {
	                enabled : false,
	            },

	            data: SLAV
	        }, {
	        	name: 'S.L ${max}',
	        	lineWidth: getElSize(15),
	        	color : "#F2F63E",
	        	 marker : {
	                 enabled : false,
	             },
	            data: SLMX
	        }, {
	        	name: 'S.L ${min}',
	        	lineWidth: getElSize(15),
	        	color : "#F63E3E",
	        	 marker : {
	                 enabled : false,
	             },
	            data: SLMN
	        }, {
	        	name: 'S.L ${current}',
	        	lineWidth: getElSize(15),
	        	color : "#9A76AF",
	        	 marker : {
	                 enabled : false,
	             },
	            data: SLCR
	        }]
	    });	
	};

	
	var className = "";
	var classFlag = true;

	function getData(){
		clearInterval(interval);
		var url = ctxPath + "/chart/getProgCd.do";
		var param = "dvcId=" + $("#dvcId").val() + 
					"&prgCd=" + $("#prgCd").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				var table = "<table>";
				
				table += "<tbody>";
				$(json).each(function(idx, data){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					table += "<tr class='" + className + "'>" + 
								"<Td style='width:8%;text-align:center' id='l" + data.line + "'' class='line'>" + data.line + "</td>" + 
								"<Td style='text-align:left;text-align:left'>" + data.cd + "</td>" +
								"<Td style='width:10%;text-align:center'></td>" + 
								"<Td style='width:10%;text-align:center'></td>" + 
								"<Td style='width:10%;text-align:center'></td>" +
							"</tr>";
				});
				
				table += "</tbody></table>";
				
				$("#table2").html(table);
				$("#table2 td").css({
					"color" : "white",
					"font-size" : getElSize(40),
					"padding" : getElSize(20),
					"border": getElSize(5) + "px solid rgb(50,50,50)"
				});
				
				$("#headerTable td").css({
					"color" : "white",
					"font-size" : getElSize(40),
					"padding" : getElSize(20),
					"border": getElSize(5) + "px solid rgb(50,50,50)"
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$(".tableContainer div:last").remove()
				
			//	scrolify($('#table'), getElSize(1500));
				
				$("*").not(".container, .page, #wrapper, .div").css({
					"overflow-x" : "hidden",
					"overflow-y" : "auto"
				});
				
				midTop = $(".line:nth(2)").offset().top;
				
				interval = setInterval(function(){
					getCurrentProg();	
				}, 1000);
			}
		});
	};

	function getToolInfo(){
		var url = ctxPath + "/chart/getToolInfo.do";
		var param = "dvcId=" + $("#dvcId").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				$("#crnTool").html(data.dataList[0].portNo);
				$("#prdCnt").html(data.dataList[0].prdCnt);
				$("#runTime").html(data.dataList[0].runTime);
				$("#limitPrdCnt").html(data.dataList[0].limitPrdCnt);
				$("#limitRunTime").html(data.dataList[0].limitRunTime);
				$("#offsetD").html(data.dataList[0].offsetD);
				$("#offsetH").html(data.dataList[0].offsetH);
				
				$("#program").html(data.dataList[0].programName);
				$("#mode").html(data.dataList[0].mode);
				$("#spdLoad").html(data.dataList[0].spdLoad);
				$("#feedOverride").html(data.dataList[0].feedOverride);
				
				$("#sec").html(data.dataList[0].sec);
				$("#avrCycleTimeSec").html(data.dataList[0].avrCycleTimeSec);
				$("#prdNo").html(data.dataList[0].prdNo);
			}
		});
	};

	var midTop;
	var preIdx= 0;
	var preDvcId = 0;
	function getCurrentProg(){
		var url = ctxPath + "/chart/getCurrentProg.do";
		var param = "dvcId=" + $("#dvcId").val() + 
					"&prgCd=" + $("#prgCd").val();
		
		if(preDvcId!=$("#prgCd").val()){
			$(".tableContainer").animate({
				"scrollTop": 0
			},10)
			
			preDvcId = $("#prgCd").val();
		};
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				getToolInfo();
				var line = "l" + data.line;
				var spdLoad = data.spdLoad;
				var feedOverride = data.feedOverride;
				var time = data.sdate.substr(11, 8);
				
				var pointer = document.createElement("div");
				
				if(preIdx!=line || preDvcId!=$("#prgCd").val()){
					console.log("change")
					$("#pointer").remove();
					
					var cLine = 0;
					$(".line").each(function(idx, data){
						if(this.id == line){
							cLine = idx;
							console.log(cLine, line)
							return false;
						}
					});
			
					var top;
					
					$(".line:nth(" + (cLine) + ")").next().next().html(spdLoad);
					$(".line:nth(" + (cLine) + ")").next().next().next().html(feedOverride);
					$(".line:nth(" + (cLine) + ")").next().next().next().next().html(time);
					
					
					if(cLine>2){
						var midTop = getElSize(535)
						
						var margin =  $(".tableContainer").offset().top - $("#table").offset().top;
						var scroll = $(".line:nth(" + (cLine - 3) + ")").offset().top - getElSize(5)
						
						$(".tableContainer").animate({
							"scrollTop": margin + 	(scroll - $(".tableContainer").offset().top) + $("#headerTable").height()
						},500, function(){
							pointer.setAttribute("id", "pointer");
							pointer.style.cssText = "position : absolute;" + 
													"width:" + $("#table tr:nth(0)").width() + "px;" + 
													"height : " + $("#table tr:nth(0)").height() + "px;" + 
													"border : " + getElSize(3) + "px solid yellow;" + 
													"z-index : 9999;" + 
													"top : " + midTop + ";" +
													"left : " + $("#" + line).offset().left + ";" ;
							
							$("body").append(pointer);
						})
					}else{
						
						$(".tableContainer").animate({
							"scrollTop": 0
						},10, function(){
							top = $("#" + line).offset().top;
							
							pointer.setAttribute("id", "pointer");
							pointer.style.cssText = "position : absolute;" + 
													"width:" + $("#table tr:nth(0)").width() + "px;" + 
													"height : " + $("#table tr:nth(0)").height() + "px;" + 
													"border : " + getElSize(3) + "px solid yellow;" + 
													"z-index : 9999;" + 
													"top : " + top + ";" +
													"left : " + $("#" + line).offset().left + ";" ;
							
							$("body").append(pointer)
						})
					}
					
					preDvcId = $("#prgCd").val();
					preIdx = line;
				}
	 		}
		});
	};

	function scroll(n){
		var cLine;
		$(".line").each(function(idx, data){
			if(this.id == "l" + n){
				cLine = idx;
				return false;
			}
		});
		
		var margin =  $(".tableContainer").offset().top - $("#table").offset().top;
		
		var scroll = $(".line:nth(" + (cLine - 3) + ")").offset().top - getElSize(5)
		
		
		$(".tableContainer").animate({
			"scrollTop": margin + 	(scroll - $(".tableContainer").offset().top)
		},500)
	};

	var interval = false;
	var SLAV = [];
	var SLMX = [];
	var SLMN = [];
	var SLCR = [];
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="bannerDiv">
	<Center>
		<input type="text" id="banner" size="100"><input type="color" id="color"><br>
		<button onclick="addBanner();">적용</button>
		<button onclick="cancelBanner();">미적용</button>
	</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<select id="dvcId">
					<option>장비</option>
					</select>
					
					<select id="prgCd">
						<option>프로그램</option>
					</select>
					
					<div class="tableContainer2">
						<table id="headerTable" style="width:100%; border-collapse: collapse;">
							<thead>
								<Tr align="center">
									<td style="width: 8%">Seq</td>
									<td>Code</td>
									<td style="width: 10%">Spindle Load</td>
									<td style="width: 10%">Feed Override</td>
									<td style="width: 10%"><spring:message code="time"></spring:message></td>
								</Tr>
							</thead>
						</table>
					</div>	
					
					<div class="tableContainer">
						<table id="table2" style="width:100%; border-collapse: collapse;">
							<thead>
								<Tr align="center">
									<td style="width: 8%">Seq</td>
									<td>Code</td>
									<td style="width: 10%">Spindle Load</td>
									<td style="width: 10%">Feed Override</td>
									<td style="width: 10%"><spring:message code="time"></spring:message></td>
								</Tr>
							</thead>
						</table>
					</div>
					
					<div id="legend">
						<Table>
							<tr>
								<Td>
									<div style="background-color: #3EC373" class="div">&nbsp;&nbsp;&nbsp;</div>	
								</Td>
								<Td>
									<font>S.L <spring:message code="avrg"></spring:message>&nbsp;&nbsp;&nbsp;</font>	
								</Td>
								<Td>
									<div style="background-color: F2F63E"  class="div">&nbsp;&nbsp;&nbsp;</div>	
								</Td>
								<Td>
									<font>S.L <spring:message code="max"></spring:message>&nbsp;&nbsp;&nbsp;</font>	
								</Td>
								<Td>
									<div style="background-color: F63E3E" class="div">&nbsp;&nbsp;&nbsp;</div>	
								</Td>
								<Td>
									<font>S.L <spring:message code="min"></spring:message>&nbsp;&nbsp;&nbsp;</font>	
								</Td>
								<Td>
									<div style="background-color: 9A76AF" class="div">&nbsp;&nbsp;&nbsp;</div>	
								</Td>
								<Td>
									<font>S.L <spring:message code="current"></spring:message>&nbsp;&nbsp;&nbsp;</font>	
								</Td>
							</tr>
						</Table>
					</div>
					
					<center>
						<div id="wrapper">
							<div id="lineChart">
							</div>				
						</div>
						<div id="noData">
							<font><spring:message code="no_data"></spring:message></font>
						</div>
					</center>
					
					<div id="lastDiv">
						<table style="width: 100%">
							<Tr>
								<td width="50%">
									<div id="toolInfo">
										<table id="toolInfoTable" style="width: 100%">
											<tr>
												<td colspan="4" class="tbleTitle" align="center">[<spring:message code="tool_info"></spring:message>]</td>
											</tr>
											<tr>
												<td width="35%" ><spring:message code="current_tool"></spring:message></td>
												<td width="15%" id="crnTool"></td>
												<td width="35%"><spring:message code="tool_status"></spring:message></td>
												<td width="15%"></td>
											</tr>
											<tr>
												<td ><spring:message code="tool_alarm"></spring:message></td>
												<td ></td>
												<td ></td>
												<td ></td>
											</tr>
											
											<tr>
												<td ><spring:message code="used_time"></spring:message></td>
												<td  id="runTime"></td>
												<td ><spring:message code="prdct_cnt"></spring:message></td>
												<td  id="prdCnt"></td>
											</tr>
											
											<tr>
												<td ><spring:message code="limit_time"></spring:message></td>
												<td  id="limitRunTime"></td>
												<td ><spring:message code="limit_cnt"></spring:message></td>
												<td  id="limitPrdCnt"></td>
											</tr>
											
											<tr>
												<td >Offset D</td>
												<td  id="offsetD"></td>
												<td >Offset H</td>
												<td  id="offsetH"></td>
											</tr>
										</table>
									</div>
								</td>
								<td>
									<div id="opInfo">
										<table id="opInfoTable" style="width: 100%">
											<tr>
												<td colspan="4" class="tbleTitle" align="center">[<spring:message code="operation_info"></spring:message>]</td>
											</tr>
											<tr>
												<td width="35%"><spring:message code="operation_prd_no"></spring:message></td>
												<td width="15%" id="prdNo"></td>
												<td width="35%">Program</td>
												<td width="15%" id="program" style="text-align: center;"></td>
											</tr>
											<tr>
												<td ><spring:message code="operation_prd_name"></spring:message></td>
												<td ></td>
												<td >Mode</td>
												<td  id="mode" style="text-align: center;"></td>
											</tr>
											
											<tr>
												<td ><spring:message code="elapse_time"></spring:message></td>
												<td  id="sec"></td>
												<td >Spindle Load</td>
												<td  id="spdLoad" style="text-align: center;"></td>
											</tr>
											
											<tr>
												<td ><spring:message code="avrg"></spring:message> Cycle Time</td>
												<td  id="avrCycleTimeSec"></td>
												<td >Feed Override</td>
												<td  id="feedOverride" style="text-align: center;"></td>
											</tr>
											<tr>
												<td > </td>
												<td ></td>
												<td >&nbsp;</td>
												<td ></td>
											</tr>
										</table>
									</div>
								</td>
							</Tr>
						</table>
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	