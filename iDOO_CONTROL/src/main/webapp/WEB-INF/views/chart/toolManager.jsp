<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
.disabledIcon{
	filter: grayscale(100%);
	-webkit-filter: grayscale(100%);
}
	
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	
	function setToolLife(){
		$("#calcStandards").css("display","block")
	};

	function showSpdLoadSet(){
		$("#calcStandards").css("display", "none");
		$("#setSpdLoad").css("display", "block");
	}

	function showPrgSet(){
		$("#calcStandards").css("display", "none");
		$("#setProg").css("display", "block");
	}

	function closeSetDiv(){
		$("#setSpdLoad, #setProg").css("display", "none");
	};

	function setSpdLoad(){
		var url = "${ctxPath}/chart/setSpdLoad.do";
		var param = "dvcId=" + $("#dvcId").val() + 
					"&zTo50=" + $("#0To50").val() + 
					"&fTo100=" + $("#50To100").val() + 
					"&over100=" + $("#over100").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success") closeSetDiv();
			}
		});
	}

	var prgArray = [];

	function setPrgSet(){
		var url = "${ctxPath}/chart/setPrgSet.do";
		var length = $("#setProg table tr").length;

		prgArray = [];
		
		for(var i = 2; i < length; i++){
			var obj = new Object();
			obj.prgCd = $("#setProg table tr:nth(" + i + ") td:nth(0)").html();
			obj.ratio = $("#setProg table tr:nth(" + i + ") td:nth(1) input").val();
			obj.dvcId = $("#dvcId").val();
			
			prgArray.push(obj)
		}
		
		var obj = new Object();
		obj.val = prgArray;
		
		var param = JSON.stringify(obj);
		
		$.ajax({
			url : url,
			data : "val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					closeSetDiv();
				}
			}
		})
	};

	
	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	function caldate(day){
		 var caledmonth, caledday, caledYear;
		 var loadDt = new Date();
		 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
		 
		 caledYear = v.getFullYear();
		 
		 if( v.getMonth() < 9 ){
		  caledmonth = '0'+(v.getMonth()+1);
		 }else{
		  caledmonth = v.getMonth()+1;
		 }
		 if( v.getDate() < 9 ){
		  caledday = '0'+v.getDate();
		 }else{
		  caledday = v.getDate();
		 }
		 return caledYear + "-" + caledmonth+ '-' + caledday;
		}
		
	
	function getDvcList(){
		var url = "${ctxPath}/chart/getDvcNameList.do";
		var param = "shopId=" + shopId + 
					"&line=ALL" + 
					"&fromDashboard=true" + 
					"&sDate=" + $("#sDate").val() + 
					"&eDate=" + $("#sDate").val() + " 23:59:59";
		
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var options = "";
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.dvcId + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>"
					//options += "<option value='" + data.id + "'>M" + decodeURIComponent(data.dvcId).replace(/\+/gi," ") + "</option>"
				});		
				
				$("#dvcId").html(options);
				getToolList();
			}, error : function(e1,e2,e3){
				console.log(e1)
			}
		});
		
	}; 

	function getCalcTy(){
		var url = "${ctxPath}/chart/getCalcTy.do";
		var param = "dvcId=" + $("#dvcId").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				if(data.isSpd=="0" || data.isSpd==null){
					$("#onlySpdLoad").prop("checked", false)
				}else{
					$("#onlySpdLoad").prop("checked", true)
				}
				
				if(data.calcTy==1 || data.calcTy==0){
					$("input[name='lifeCalcStandard']:nth(0)").prop("checked", true)
				}else{
					$("input[name='lifeCalcStandard']:nth(1)").prop("checked", true)
				}
			}
		});
	};

	function getSpdLoadInfo(){
		$("#dvcName").html($("#dvcId option:selected").text());

		var url = "${ctxPath}/chart/getSpdLoadInfo.do";
		var param = "dvcId=" + $("#dvcId").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				$("#0To50").val(data.zto50);
				$("#50To100").val(data.fto100);
				$("#over100").val(data.over100);
				
				if(data.zto50==null) $("#0To50").val(0);
				if(data.fto100==null) $("#50To100").val(0);
				if(data.over100==null) $("#over100").val(0);
			}
		});
	};

	function getProgInfo(){
		var url = "${ctxPath}/chart/getProgInfo.do";
		var param = "dvcId=" + $("#dvcId").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				var tr = "<Tr>" +
							"<td colspan='2'>" + $("#dvcId option:selected").html() + "</td>" +
							"</Tr>" +
							"<Tr>" +
								"<Td>${program}</Td><td>${operation}(%)</td>" +
						"</Tr>";
			
				
				$(json).each(function(idx, data){
					tr += "<tr>" + 
								"<td>" + data.prgCd +"</td>" +
								"<td><input type='text' size='5' value=" + data.ratio + "></td>" + 
							"</tr>";
				});
				
				$("#setProg table").html(tr);
				setEl();
			}
		});
	};


	var className = "";
	var classFlag = true;
	function getToolList(){
		getSpdLoadInfo();
		getCalcTy();
		getProgInfo();
		
		var url = "${ctxPath}/chart/getToolList.do";
		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val()
		
		var param = "sDate=" + sDate +
					"&eDate=" + eDate + " 23:59:59" + 
					"&dvcId=" + $("#dvcId").val();
		
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var tr = "<thead><Tr class='tr_table_fix_header' style=\"font-weight: bolder;background-color: rgb(34,34,34)\" class=\"thead\">" + 
							"<td rowspan=\"2\">Tool No</td>" + 
							"<td rowspan=\"2\" width=\"5%\">Spec</td>" + 
							"<td rowspan=\"2\">Port No</td>" + 
							"<td rowspan=\"2\">Status</td>" +
							"<td colspan=\"4\">Tool Cycle Count</td>" +
							"<td colspan=\"4\">RunTime (h)</td>" + 
							"<td colspan=\"3\">Offset D</td>" + 
							"<td colspan=\"3\">Offset H</td>" +
							"<td rowspan=\"2\">Reset Date</td>" + 
						"</Tr>" + 
						"<tr  class='tr_table_fix_header' style=\"font-weight: bolder;background-color: rgb(34,34,34)\" class=\"thead\">" +
							"<td>LMT</td>" + 
							"<td>Current</td>" + 
							"<td>Remain</td>" + 
							"<td>Used rate(%)</td>" +
							"<td>LMT</td>" + 
							"<td>Current</td>" + 
							"<td>Remain</td>" + 
							"<td>Used rate(%)</td>" + 
							"<td>LMT</td>" + 
							"<td>Init</td>" + 
							"<td>Current</td>" + 
							"<td>LMT</td>" + 
							"<td>Init</td>" + 
							"<td>Current</td>" + 
						"</tr></thead>";
						
				
				csvOutput = "Tool No,Spec, Port No, Cycle Limited, Cycle Current , Cycle Remain, Cycle Used rate(%),RunTime Limited, RunTime Current , RunTime Remain, RunTime Used rate(%), Offset D Limited, Offset D Initial, Offset D Current, 	Offset H Limited, Offset H Initial, Offset H CurrentLINE";

				initMap = new JqMap();
				initArray = [];
				$("#save, #undo").addClass("disabledIcon");
				$(json).each(function(idx, data){
					var status = "green";
					
					if(Number(data.runTimeUsedRate)>=90){
						status = "yellow";
					}
					
					if(Number(data.cycleUsedRate)>=90){
						status = "yellow";
					}
					
					if((Number(data.offsetDCurrent)/(Number(data.offsetDLimit) - Number(data.offsetDInit)))*100>=50){
						status = "yellow";
					}
					
					if((Number(data.offsetHCurrent)/(Number(data.offsetHLimit) - Number(data.offsetHInit)))*100>=50){
						status = "yellow";
					}
					
					if(Number(data.offsetDLimit) < (Number(data.offsetDInit) + Number(data.offsetDCurrent))){
						status = "red";
					};
					
					if(Number(data.offsetHLimit) < (Number(data.offsetHInit) + Number(data.offsetHCurrent))){
						status = "red";
					};
					
					if(data.prdCntLmt=="-" && data.runTimeLimit == "-" && Number(data.offsetDLimit) == 0 && Number(data.offsetHLimit) == 0){
						status = "gray";
					};
		
					if(data.cycleRemain<0 || data.runTimeRemain<0){
						status = "red";
					}
					
					initMap.put("spec__" + data.portNo, data.spec);
					initMap.put("prd_cnt_lmt__" + data.portNo,data.prdCntLmt);
					initMap.put("rn_tm_lmt_sec__" + data.portNo,data.runTimeLimit);
					initMap.put("of_d_lmt__" + data.portNo,data.offsetDLimit);
					initMap.put("of_h_lmt__" + data.portNo,data.offsetHLimit);
					
					initArray.push(data.spec);
					initArray.push(data.prdCntLmt);
					initArray.push(data.runTimeLimit);
					initArray.push(data.offsetDLimit);
					initArray.push(data.offsetHLimit);
					
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					tr += "<tbody><tr class='" + className + "'>" + 
								"<td>" + data.toolNm + "</td>" +
								"<td><input type='text' size='5' value='" + data.spec + "'  id='spec__" + data.portNo +"'></td>" +
								"<td>" + data.portNo + "</td>" +
								"<td align='center'>" + "<div onclick = chkReset('" + data.portNo + "','" + data.date + "'); style='border-radius:50%; cursor : pointer; background-color : " + status + "; width: " + getElSize(80) + "px; height:" + getElSize(80) + "px'></div>" + "</td>" +
								"<td><input type='text' value='" + data.prdCntLmt + "' size='5' id='prd_cnt_lmt__" + data.portNo +"'></td>" +
								"<td>" + data.prdCntCrt + "</td>" +
								"<td>" + data.cycleRemain + "</td>" +
								"<td>" + data.cycleUsedRate + "</td>" +
								"<td><input type='text' value='" + data.runTimeLimit + "' id='rn_tm_lmt_sec__" + data.portNo + "'size='5'></td>" +
								"<td>" + data.runTimeCurrent + "</td>" +
								"<td>" + data.runTimeRemain + "</td>" +
								"<td>" + data.runTimeUsedRate + "</td>" +
								"<td><input type='text' value='" + data.offsetDLimit + "' id='of_d_lmt__" + data.portNo + "' size='5'></td>" +
								"<td>" + data.offsetDInit + "</td>" +
								"<td>" + data.offsetDCurrent + "</td>" +
								"<td><input type='text' value='" + data.offsetHLimit + "' id='of_h_lmt__" + data.portNo + "' size='5'></td>" +
								"<td>" + data.offsetHInit + "</td>" +
								"<td>" + data.offsetHCurrent + "</td>" +
								"<td>" + data.resetDt  + "</td>" +
						"</tr></tbody>";
						
					csvOutput += data.toolNm + "," + 
								data.spec + "," + 
								data.portNo + "," + 
								data.prdCntLmt + "," +
								data.prdCntCrt + "," + 
								data.cycleRemain + "," +
								data.cycleUsedRate + "," +
								data.runTimeLimit + "," +
								data.runTimeCurrent + "," + 
								data.runTimeRemain + "," +
								data.runTimeUsedRate + "," + 
								data.offsetDLimit + "," + 
								data.offsetDInit + "," + 
								data.offsetDCurrent + "," + 
								data.offsetHLimit + "," + 
								data.offsetHInit + "," + 
								data.offsetHCurrent + "," + 
								data.resetDt  +"LINE";
				});
				
				$("#table2").html(tr);
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});

				
				
				$("#table2 td").css({
					"font-size" : getElSize(50),
					"border" : getElSize(7) + "px solid black"
				});
				
				$("#table2 input[type=text]").keyup(function(){
					currentInitArray = [];
					currentInitMap = new JqMap();
					$("#table2 input[type=text]").each(function(idx, data){
						var id = data.id;
						var val = $(data).val();
						
						currentInitMap.put(id, val);
						currentInitArray.push(val)
						
					});
					
					if(currentInitArray.toString()==initArray.toString()){
						$("#save, #undo").addClass("disabledIcon");
					}else{
						$("#save, #undo").removeClass("disabledIcon");					
					}
				});  
				
				//bindEvt();
				
				$("#table2 input").focus(function(){
					preEl = $(this);
					preVal = $(this).val();
					$(this).val("")
				});
				
				$("#table2 input").blur(function(){
					if($(this).val()=="") preEl.val(preVal)
			 	});
				
				//$("#container div:last").remove();
				//scrolify($('#table2'), getElSize(1400));
				//$("#container div:last").css("overflow", "auto");
		
				if($(".tmpTable").length>2){
					$("table:last").remove();
				}
			}	
		}); 
	};

	var initMap = new JqMap();
	var currentInitMap = new JqMap();
	var initArray = [];
	var currentInitArray = [];
	var selected_prtNo, selected_date;

	function chkReset(portNo, date){
		$("#resetDiv").css({
			"z-index" : 9,
			"display" : "block"
		});
		
		selected_prtNo = portNo;
		selected_date = date;
	};

	function chkSave(){
		$("#saveDiv").css({
			"z-index" : 9,
			"display" : "block"
		});
	};

	function noReset(){
		$("#resetDiv, #saveDiv, #resetCurrentDiv").css({
			"z-index" : -1,
			"display" :"none"
		});
	};

	var cycleCntCurrent;
	var runTimeCurrent;

	function showSetCurrentVal(){
		$("#resetDiv, #saveDiv").css({
			"z-index" : -1,
			"display" :"none"
		});
		
		$("#resetCurrentDiv").css({
			"z-index" : 9,
			"display" : "block"
		});
	};

	function okReset(){
		var url = ctxPath + "/chart/resetTool.do";
		
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		
		var param = "portNo=" + selected_prtNo + 
					"&dvcId=" + $("#dvcId").val() + 
					"&sDate=" + today + 
					"&prdCntCrt=" + $("#resetCycleCntCrnt").val() + 
					"&rnTmCrtSec=" + $("#rnTmCrnt").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				noReset();
				getToolList();
			}
		});
	}
	
	function bindEvt2(){
		$("#table2 input").focus(function(){
			preEl = $(this);
			preVal = $(this).val();
			$(this).val("")
		});
		
		$("#table2 input").blur(function(){
			if($(this).val()=="") preEl.val(preVal)
	 	});
	};

	
	function undoLimit(){
		for(var key in initMap.map){
			if (initMap.map.hasOwnProperty(key)) {
				$("#" + key).val(initMap.map[key])
			}
			$("#save, #undo").addClass("disabledIcon");
		}
	};

	var changedLimit = [];
	function saveLimit(){
		changedLimit = [];
		for(var key in initMap.map){
			if (initMap.map.hasOwnProperty(key)) {
				if(initMap.map[key]!=currentInitMap.map[key]){
					var array = [key, currentInitMap.map[key]]
					changedLimit.push(array)
				}
			}
		}
		
		setLimit();
	};

	var limitIdx = 0;
	function setLimit(){
		var limit = changedLimit[limitIdx];
		var dvcId = $("#dvcId").val();
		var item = limit[0].substr(0, limit[0].lastIndexOf("__"));
		var portNo = limit[0].substr(limit[0].lastIndexOf("__")+2);
		var value = limit[1];
		
		if(item=="rn_tm_lmt_sec"){
			value *= 3600;
		}
		
		if(value=="-") value = Number(null);
		
		var url = "${ctxPath}/chart/setLimit.do";
		var param = "dvcId=" + dvcId + 
					"&item=" + item + 
					"&portNo=" + portNo + 
					"&value=" + value;
		
		console.log(param);
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(){
				limitIdx++;
				if(limitIdx<changedLimit.length){
					setLimit();	
				}else{
					limitIdx = 0;
					$("#save, #undo").addClass("disabledIcon");
					getToolList();
					$("#saveDiv").css("z-index", -10);
				}
			}
		});
	};


	function setCalcTy(){
		var url = "${ctxPath}/chart/setCalcTy.do";
		var isCheck = $("#onlySpdLoad").prop("checked");
		if(isCheck){
			isCheck = 1;
		}else{
			isCheck = 0;
		}
		
		var param = "dvcId=" + $("#dvcId").val() + 
					"&calcTy=" + $("input[name='lifeCalcStandard']:checked").val() + 
					"&isSpd=" + isCheck;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					$("#calcStandards").css("display", "none")
				}
			}		
		});
	};

	var preVal = "";
	var preEl = "";
	
	var handle = 0;
	$(function(){
		bindEvt2();
		$("#undo").click(undoLimit);
		$("#save").click(chkSave);
		
		createNav("tool_nav", 0);
		setDate();	
		time();
		
		$("#home").click(function(){ location.href = "/iDOO_CONTROL/chart/index.do" });
		setEl();
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		getDvcList();	
		$(".date").change(getToolList);
		$("#dvcId").change(getToolList);
		chkBanner();
	});
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").not("#intro").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50),
		});
		
		$("#content").css({
			"height" : getElSize(1500),
			"overflow" : "auto"
		});
		
		$("select, button, input").css({
			"font-size" : getElSize(50),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#chart").css({
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20),
			"margin-top" : getElSize(20),
		});
		
		$("#arrow_left").css({
			"width" : getElSize(60),
			"margin-right" : getElSize(50), 
			"cursor" : "pointer"
		});
		
		$("#arrow_right").css({
			"width" : getElSize(60),
			"margin-left" : getElSize(50), 
			"cursor" : "pointer"
		});
		
		
		$("#save, #undo").css({
			"width" : getElSize(100),
			"cursor" : "pointer"
		});
		
		$("#save, #undo").addClass("disabledIcon");
		
		$("#icons td").css({
			"color" : "#8D8B72"
		})
		
		$("#setProg").css({
			"width" : getElSize(1000),
			"position" :"absolute",
			"z-index" : 9,
			"background-color" : "#444444",
			"padding" : getElSize(20),
			"display" : "none"
		});
		
		$("#setSpdLoad").css({
			"width" : getElSize(1000),
			"position" :"absolute",
			"z-index" : 9,
			"background-color" : "#444444",
			"padding" : getElSize(20),
			"display" : "none"
		});
		
		$("#calcStandards").css({
			"width" : getElSize(1000),
			"position" :"absolute",
			"z-index" : 9,
			"background-color" : "#444444",
			"padding" : getElSize(20),
			"display" : "none"
		});
		
		$("#calcStandards, #setSpdLoad, #setProg").css({
			"top" : (window.innerHeight/2) - ($("#calcStandards").height()/2),
			"left" : (window.innerWidth/2) - ($("#calcStandards").width()/2),
		});
		
		$("#calcStandards table td, #setSpdLoad table td,  #setProg table td").css({
			"color" : "white",
			"font-size" : getElSize(50),
			"text-align" : "Center",
			"border" : getElSize(2) + "px solid white"
		});
		
		$("#calcStandards table td button").css({
			"font-size" : getElSize(50),
			"width" : getElSize(700)
		});
		
		$("#close_btn").css({
			"width" : getElSize(80),
			"position" :"absolute",
			"top" : 0,
			"right" : 0,
			"cursor" : "pointer"
		}).click(function(){
			$("#calcStandards").css("display", "none")
		});
		
		$("#resetDiv, #denyResetDiv, #saveDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#resetCurrentDiv").css({
			"position" : "absolute",
			"width" : getElSize(900),
			"height" :getElSize(280),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#resetCurrentDiv table td").css({
			"padding" :getElSize(20),
			"color" : "white",
			"font-size" : getElSize(40)
		});
		
		$("#resetCurrentDiv").css({
			"top" : (window.innerHeight/2) - ($("#resetCurrentDiv").height()/2),
			"left" : (window.innerWidth/2) - ($("#resetCurrentDiv").width()/2),
			"z-index" : -99,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#resetDiv, #denyResetDiv, #saveDiv").css({
			"top" : (window.innerHeight/2) - ($("#resetDiv").height()/2),
			"left" : (window.innerWidth/2) - ($("#resetDiv").width()/2),
			"z-index" : -99,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		
		$("#resetDiv button, #resetDiv span, #saveDiv button, #saveDiv span, #denyResetDiv button, #denyResetDiv span").css({
			"font-size" :getElSize(40)
		});
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function goGraph(){
		location.href = "${ctxPath}/chart/performanceReport.do"
	};
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="setProg">
		<table style="width: 100%">
		</table>
		<center>
			<button onclick="setPrgSet();"><spring:message code="save"></spring:message> </button> <button onclick="closeSetDiv()"><spring:message code="cancel"></spring:message></button>
		</center>		
	</div>

	<div id="setSpdLoad">
		<table style="width: 100%">
			<Tr>
				<td id="dvcName" colspan="3"></td>
			</Tr>
			<Tr>
				<Td colspan="2"><spring:message code="spd_load_range"></spring:message></Td><td rowspan="2"><spring:message code="ratio"></spring:message> (%)</td>
			</Tr>
			<Tr>		
				<Td><spring:message code="unnormal"></spring:message></Td>
				<Td><spring:message code="under"></spring:message></Td>
			</Tr>
			<tr>
				<td>0</td> <td>50</td> <td><input type="text" id='0To50' size="5">  </td> 
			</tr>
			<tr>
				<td>50</td> <td>100</td><Td><input type="text" id='50To100'size="5">  </td>
			</tr>
			<tr>
				<td>100</td> <td>9999</td><td><input type="text" id='over100' size="5">  </td>
			</tr>
		</table>
		<center>
			<button onclick="setSpdLoad()"><spring:message code="save"></spring:message></button> <button onclick="closeSetDiv()"><spring:message code="cancel"></spring:message></button>
		</center>		
	</div>
	<div id="calcStandards">
		<img alt="" src="${ctxPath }/images/close_btn.png" id="close_btn">
		<table style="width: 100%">
			<tr>
				<td align="right" ><!-- <input type="radio" name="lifeCalcStandard" value="1" > --><input type="checkbox" id="onlySpdLoad"> </td><td><spring:message code="spd_load_only"></spring:message></td>
			</tr>
			<tr>
				<Td  align="right"><input type="radio" name="lifeCalcStandard" value="1"></td><td><button onclick="showSpdLoadSet()"><spring:message code="spd_load_only2"></spring:message></button> </Td>
			</tr>
			<Tr>
				<Td align="right"><input type="radio" name="lifeCalcStandard" value="2"></td><td><button onclick="showPrgSet();"><spring:message code="program_addition"></spring:message></button> </Td>
			</Tr>
		</table>	
		<center><button onclick="setCalcTy()"><spring:message code="check"></spring:message></button></center>
	</div>

	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/tool_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">			
					<div  id="dvcDiv">
						<table id="icons" style="width: 100%">
							<tr>
								<Td>
									<spring:message code="device"></spring:message>
								</Td>
								<Td>
									<select id="dvcId" ></select> <input type="date" class="date" id="sDate" style="display: none"> <input type="date" class="date" id="eDate" style="display: none">
								</Td>
								<Td>
									<font id="statusInfo"><spring:message code="status_icon_click_label"></spring:message></font>
								</Td>
								<Td  style="text-align: right;">
									<img src="${ctxPath }/images/save.png" id="save"> 
									<img src="${ctxPath }/images/undo.png" id="undo">
								</Td>
								<td style="text-align: right; width: 25%">
									<button onclick="setToolLife()" id="stTlLf"><spring:message code="add_tool_cal_standard"></spring:message></button>
									<button><spring:message code="excel"></spring:message> </button>							
								</td>
							</tr>
						</table>
					</div>
					<div id="content">
						<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="tmpTable" border="1" id="table2">
						</table>					
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath}/images/unselected.png " class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="saveDiv">
		<Center>
			<font><spring:message code="check_save"></spring:message> </font><Br><br>
			<button id="resetOk" onclick="saveLimit();"><spring:message code="confirm"></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noReset();"><spring:message code="cancel"></spring:message></button>
		</Center>
	</div>
	
	<div id="resetDiv">
		<Center>
			<font><spring:message code="chkReset"></spring:message></font><Br><br>
			<button id="resetOk" onclick="showSetCurrentVal();"><spring:message code="confirm"></spring:message> </button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noReset();"><spring:message code="cancel"></spring:message></button>
		</Center>
	</div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
	<div id="resetCurrentDiv">
		<Center>
			<table style="border-collapse: collapse; width: 100%" border="1">
				<Tr>
					<Td align="center" width="50%">Tool Cycle Count</Td>
					<Td align="center">RunTime (h)	</Td>
				</Tr>
				<Tr>
					<td align="center"><input type="text" id="resetCycleCntCrnt" size="5" value="0">  </td>
					<td align="center"><input type="text" id="rnTmCrnt" size="5" value="0"> </td>
				</Tr>			
				<tr>
					<td colspan="2" align="center"><button onclick="okReset()"><spring:message code="check"></spring:message></button> </td>
				</tr>
			</table>
		</Center>
	</div>
</body>
</html>	