<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	
	
	function getGroup(){
		var url = "${ctxPath}/chart/getJigList.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + decode(data.jig) + "'>" + decode(data.jig) + "</option>"; 
				});
				
				$("#group").html(option);
				
				getTableData("jig");
			}
		});
	};

	var jigCsv;
	var wcCsv;
	var selected_dvc;
	var className = "";
	var classFlag = true;
	var preLine;
	var sum_target_op_time = 0;
		sum_total_op_time = 0;
		sum_op_date = 0;
		sum_incycle = 0;
		sum_wait = 0;
		sum_alarm = 0;
		sum_noConn = 0;
		

	var dvc;

	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function showWcData(name,sDate, eDate, ty){
		window.location.hash = "pop"
		selected_dvc = name;
		dvc = replaceHash(name);
		var url = "${ctxPath}/chart/getWcData.do";
		var param = "&sDate=" + sDate + 
					"&eDate=" + eDate + 
					"&name=" + name;;

		
		if(ty=="jig"){
			$("#wc_sdate").val($("#jig_sdate").val());
			$("#wc_edate").val($("#jig_edate").val());		
		};
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.wcList;
				
				$(".contentTr2").remove();
				var tr = "<tbody>";
				wcData = "${device},${target_op_time},${date},${incycle},${wait},${alarm},${noconnection},${opratio_against_target}LINE";
				$(json).each(function(idx, data){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
					var wait = Number(Number(data.wait_time/60/60).toFixed(1));
					var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
					var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
					
					tr += "<tr class='contentTr2 " + className + "')'>" +
								"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" +
								"<td>" + Number(data.target_time/60/60).toFixed(1) + "</td>" + 
								"<td>" + data.workDate + "</td>" + 
								"<td>" + incycle + "</td>" +
								"<td>" + wait + "</td>" + 
								"<td>" + alarm + "</td>" +
								"<td>" + noconn + "</td>" + 
								"<td>" + Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%</td>" + 
						 "</tr></tbody>";
						 
					
					wcData += data.name + "," + 
							Number(data.target_time/60/60).toFixed(1) + "," + 
							data.workDate + "," +
							incycle + "," + 
							wait + "," +
							alarm + "," +
							noconn + "," +
							Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%LINE";
				});
				tr += "</tbody>";
				$("#wcTable").append(tr);
				setEl();
				//getDvcList(name);
				
				$("#corver").click(function(){
					//clearMenu();
					//location.href = "${ctxPath}/chart/performanceReport.do";
				});
				
				$("#corver").css({
					"z-index" : 9,
					"opacity" : 0.7
				});
				
				$("#dlgTable").css({
					"z-index":10,
					"display" : "block"
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$(".tableContainer2 div:last").remove();
				scrolify($('#wcTable'), getElSize(1000));
				
				$("*").css({
					"overflow-x" : "hidden",
					"overflow-y" : "auto"
				})
				
				$("#dvcDiv2").css({
					"margin-left" : getElSize(100),
					"width" : $(".tableContainer2").width(),
					"margin-bottom" : getElSize(50)
				})
			}
		});
	};

	function showWcDatabyDvc(dvc, sDate, eDate, ty){
		var url = "${ctxPath}/chart/getWcDataByDvc.do";
		var param = "name=" + dvc + 
					"&sDate=" + sDate + 
					"&eDate=" + eDate + 
					"&shopId=" + shopId;

		//$("#wcSelector").html(wc);
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.wcList;
				
				$(".contentTr2").remove();
				var tr = "";
				wcData = "职务,WC,WC GROUP,目标运转时间,日子,运转,待机,中断,断电,相比目标运转率LINE";
				$(json).each(function(idx, data){
					var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
					var wait = Number(Number(data.wait_time/60/60).toFixed(1));
					var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
					var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
					
					tr += "<tr class='contentTr2')'>" +  
								"<td>" + Number(data.target_time/60/60).toFixed(1) + "</td>" + 
								"<td>" + data.workDate + "</td>" + 
								"<td>" + incycle.toFixed(1) + "</td>" +
								"<td>" + wait + "</td>" + 
								"<td>" + alarm + "</td>" +
								"<td>" + noconn + "</td>" + 
								"<td>" + Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%</td>" + 
						 "</tr>";
						 
					
					wcData += 
							Number(data.target_time/60/60).toFixed(1) + "," + 
							data.workDate + "," +
							Number(data.inCycle_time/60/60).toFixed(1) + "," + 
							Number(data.wait_time/60/60).toFixed(1) + "," +
							Number(data.alarm_time/60/6).toFixed(1) + "," +
							Number(data.noConnTime/60/60).toFixed(1) + "," +
							Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%LINE";
				});
				
				$("#wcTable").append(tr);
				setEl();
			}
		});
	};



	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	function goGraphPage(){
		var type = this.id;
		var url;
		if(type=="jig"){
			url = "${ctxPath}/chart/jigGraph.do?group=" + $("#group").val();
		}else{
			var sDate = $("#wc_sdate").val();
			var eDate = $("#wc_edate").val();
			url = "${ctxPath}/chart/dailyChart.do?sDate=" + sDate + "&eDate=" + eDate + "&name=" + dvc;
		};
		location.href = url;
	};

	var jigCsv;
	var wcCsv;
	var selected_dvc;
	var className = "";
	var classFlag = true;
	var preLine;
	var sum_target_op_time = 0;
		sum_total_op_time = 0;
		sum_op_date = 0;
		sum_incycle = 0;
		sum_wait = 0;
		sum_alarm = 0;
		sum_noConn = 0;
		
	function getTableData(el){
		var id = this.id;

		var sDate;
		var eDate;
		var ty;
		var url = "${ctxPath}/chart/getTableData.do";
		if(id=="jig_sdate" || id=="jig_edate" || el=="jig"){
			sDate = $("#jig_sdate").val();
			eDate = $("#jig_edate").val();
			
			window.localStorage.setItem("jig_sDate", sDate);
			window.localStorage.setItem("jig_eDate", eDate);
			
		}else{
			sDate = $("#wc_sdate").val();
			eDate = $("#wc_edate").val();
			
			showWcData(selected_dvc,sDate, eDate);
			
			window.localStorage.setItem("wc_sDate", sDate);
			window.localStorage.setItem("wc_eDate", eDate);
			
			return;
		};
		
		var param = "sDate=" + sDate + 
					"&eDate=" + eDate + 
					"&ty=" +ty +
					"&shopId=" + shopId + 
					"&jig=" + $("#group").val();
		
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.tableData;
				
				var start = new Date(sDate);
				var end = new Date(eDate);
				var n = (end - start)/(24 * 3600 * 1000)+1;

				$(".contentTr").remove();
				var tr = "<tbody>";
				jigData = "${device},${target_op_time},${total_op_time},${number_of_op_day},${incycle},${wait},${alarm},${noconnection},${opratio_against_target}LINE";
				
				var sum_target_op_time = 0;
				var sum_total_op_time = 0;
				var sum_op_date = 0;
				var sum_incycle = 0;
				var sum_wait = 0;
				var sum_alarm = 0;
				var sum_noConn = 0;
				var sum_opRatio = 0;
				var dvcCnt = 0;
				preLine = "";
				$(json).each(function(idx, data){
					if(preLine != data.jig && idx!=0){
						tr += "<tr>" + 
								"<td>" + decode(preLine) + " (소계)</td>" + 
								"<td></td>" +
								"<td></td>" +
								"<td></td>" +
								"<td>" + Math.round(sum_target_op_time) + "</td>" +
								"<td>" + Math.round(sum_total_op_time) + "</td>" + 
								"<td>" + Math.round(sum_op_date) + "</td>" + 
								"<td>" + Math.round(sum_incycle) + "</td>" +
								"<td>" + Math.round(sum_wait) + "</td>" + 
								"<td>" + Math.round(sum_alarm) + "</td>" +
								"<td>" + Math.round(sum_noConn) + "</td>" + 
								"<td>" + (sum_opRatio/dvcCnt).toFixed(1) + "%</td>" + 
							"</tr>";
							
						sum_target_op_time = 0;
						sum_total_op_time = 0;
						sum_op_date = 0;
						sum_incycle = 0;
						sum_wait = 0;
						sum_alarm = 0;
						sum_noConn = 0;
						sum_opRatio = 0;
						
						dvcCnt = 0;
					}
					preLine = data.jig;
					dvcCnt++;
					
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					var incycle = Number(Number(data.inCycle_time/60/60/n).toFixed(1));
					var wait = Number(Number(data.wait_time/60/60/n).toFixed(1));
					var alarm = Number(Number(data.alarm_time/60/60/n).toFixed(1));
					var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
					
					tr += "<tr class='contentTr " + className + "' ondblclick='showWcData(\"" + data.name + "\","  + "\"" + sDate + "\"," +  "\"" + eDate + "\"" + ",\"jig\")'>" +
								"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" +
								"<td>" + decodeURIComponent(data.jig).replace(/\+/gi, " ") + "</td>" +
								"<td>" + decodeURIComponent(data.WC).replace(/\+/gi, " ") + "</td>" +
								"<td>" + decodeURIComponent(data.WCG).replace(/\+/gi, " ") + "</td>" +
								"<td>" + Number(data.target_time/60/60).toFixed(1) + "</td>" + 
								"<td>" + Number(data.inCycle_time/60/60).toFixed(1) + "</td>" + 
								"<td>" + n + "</td>" + 
								"<td>" + incycle + "</td>" +
								"<td>" + wait + "</td>" + 
								"<td>" + alarm + "</td>" +
								"<td>" + noconn + "</td>" + 
								"<td>" + calcTargetRatio(data.target_time, data.inCycle_time) + "%</td>" + 
						 "</tr>";
						 
					jigData += data.name + "," + 
							Number(data.target_time/60/60).toFixed(1) + "," +  
							Number(data.inCycle_time/60/60).toFixed(1) + "," + 
							n + "," + 
							incycle + "," + 
							wait + "," + 
							alarm + "," + 
							noconn + "," + 
							Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%LINE";
					
					sum_target_op_time += Number(Number(data.target_time/60/60).toFixed(1));
					sum_total_op_time += Number(Number(data.inCycle_time/60/60).toFixed(1));
					sum_op_date += Number(n);
					sum_incycle += Number(incycle);
					sum_wait += Number(wait);
					sum_alarm += Number(alarm);
					sum_noConn += Number(noconn);	
					sum_opRatio += calcTargetRatio(data.target_time, data.inCycle_time);
				});

				if(tr=="<tbody>"){
					tr+= "<tr>" + 
							"<td colspan='12'>데이터가 없습니다.</td>" + 
						"</tr></tbody>";		
				}else{
					tr+= "<tr>" + 
						"<td>" + decode(preLine) + " (소계)</td>" + 
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"<td>" + Math.round(sum_target_op_time) + "</td>" +
						"<td>" + Math.round(sum_total_op_time) + "</td>" + 
						"<td>" + Math.round(sum_op_date) + "</td>" + 
						"<td>" + Math.round(sum_incycle) + "</td>" +
						"<td>" + Math.round(sum_wait) + "</td>" + 
						"<td>" + Math.round(sum_alarm) + "</td>" +
						"<td>" + Math.round(sum_noConn) + "</td>" + 
						"<td>" + (sum_opRatio/dvcCnt).toFixed(1) + "%</td>" + 
					"</tr></tbody>";	
				}
				
				
				$("#jigTable").append(tr);
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				setEl();
				
				$(".tableContainer div:last").remove()
				
				scrolify($('#jigTable'), getElSize(1400));
				
				$("*").not("#dvcDiv").css({
					"overflow-x" : "hidden",
					"overflow-y" : "auto"
				});
				
			}
		});
	};

	var dvc;

	function calcTargetRatio(targetTime, incycleTime){
		return (targetTime == null || targetTime == 0) ? 0 : Number((incycleTime/(targetTime)) * 100).toFixed(1);
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function showWcData(name,sDate, eDate, ty){
		window.location.hash = "pop"
		selected_dvc = name;
		dvc = replaceHash(name);
		var url = "${ctxPath}/chart/getWcData.do";
		var param = "&sDate=" + sDate + 
					"&eDate=" + eDate + 
					"&name=" + name;;

		
		if(ty=="jig"){
			$("#wc_sdate").val($("#jig_sdate").val());
			$("#wc_edate").val($("#jig_edate").val());		
		};
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.wcList;
				
				$(".contentTr2").remove();
				var tr = "<tbody>";
				wcData = "${device},${target_op_time},${date},${incycle},${wait},${alarm},${noconnection},${opratio_against_target}LINE";
				$(json).each(function(idx, data){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
					var wait = Number(Number(data.wait_time/60/60).toFixed(1));
					var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
					var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
					
					tr += "<tr class='contentTr2 " + className + "')'>" +
								"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" +
								"<td>" + Number(data.target_time/60/60).toFixed(1) + "</td>" + 
								"<td>" + data.workDate + "</td>" + 
								"<td>" + incycle + "</td>" +
								"<td>" + wait + "</td>" + 
								"<td>" + alarm + "</td>" +
								"<td>" + noconn + "</td>" + 
								"<td>" + Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%</td>" + 
						 "</tr></tbody>";
						 
					
					wcData += data.name + "," + 
							Number(data.target_time/60/60).toFixed(1) + "," + 
							data.workDate + "," +
							incycle + "," + 
							wait + "," +
							alarm + "," +
							noconn + "," +
							Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%LINE";
				});
				tr += "</tbody>";
				$("#wcTable").append(tr);
				setEl();
				//getDvcList(name);
				
				$("#corver").click(function(){
					//clearMenu();
					//location.href = "${ctxPath}/chart/performanceReport.do";
				});
				
				$("#corver").css({
					"z-index" : 9,
					"opacity" : 0.7
				});
				
				$("#dlgTable").css({
					"z-index":10,
					"display" : "block"
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$(".tableContainer2 div:last").remove();
				scrolify($('#wcTable'), getElSize(1300));
				
				$("*").css({
					"overflow-x" : "hidden",
					"overflow-y" : "auto"
				})
				
				$("#dvcDiv2").css({
					"margin-left" : getElSize(100),
					"width" : $(".tableContainer2").width(),
					"margin-bottom" : getElSize(50)
				})
			}
		});
	};

	function showWcDatabyDvc(dvc, sDate, eDate, ty){
		var url = "${ctxPath}/chart/getWcDataByDvc.do";
		var param = "name=" + dvc + 
					"&sDate=" + sDate + 
					"&eDate=" + eDate + 
					"&shopId=" + shopId;

		//$("#wcSelector").html(wc);
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.wcList;
				
				$(".contentTr2").remove();
				var tr = "";
				wcData = "职务,WC,WC GROUP,目标运转时间,日子,运转,待机,中断,断电,相比目标运转率LINE";
				$(json).each(function(idx, data){
					var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
					var wait = Number(Number(data.wait_time/60/60).toFixed(1));
					var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
					var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
					
					tr += "<tr class='contentTr2')'>" +  
								"<td>" + Number(data.target_time/60/60).toFixed(1) + "</td>" + 
								"<td>" + data.workDate + "</td>" + 
								"<td>" + incycle.toFixed(1) + "</td>" +
								"<td>" + wait + "</td>" + 
								"<td>" + alarm + "</td>" +
								"<td>" + noconn + "</td>" + 
								"<td>" + Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%</td>" + 
						 "</tr>";
						 
					
					wcData += 
							Number(data.target_time/60/60).toFixed(1) + "," + 
							data.workDate + "," +
							Number(data.inCycle_time/60/60).toFixed(1) + "," + 
							Number(data.wait_time/60/60).toFixed(1) + "," +
							Number(data.alarm_time/60/6).toFixed(1) + "," +
							Number(data.noConnTime/60/60).toFixed(1) + "," +
							Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%LINE";
				});
				
				$("#wcTable").append(tr);
				setEl();
			}
		});
	};



	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	function goGraphPage(){
		var type = this.id;
		var url;
		if(type=="jig"){
			url = "${ctxPath}/chart/jigGraph.do?group=" + $("#group").val();
		}else{
			var sDate = $("#wc_sdate").val();
			var eDate = $("#wc_edate").val();
			url = "${ctxPath}/chart/dailyChart.do?sDate=" + sDate + "&eDate=" + eDate + "&name=" + dvc;
		};
		location.href = url;
	};
	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$("#jig_sdate").val(caldate(7));
		$("#jig_edate").val(year + "-" + month + "-" + day);
	};
	
	function caldate(day){
		 var caledmonth, caledday, caledYear;
		 var loadDt = new Date();
		 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
		 
		 caledYear = v.getFullYear();
		 
		 if( v.getMonth() < 9 ){
		  caledmonth = '0'+(v.getMonth()+1);
		 }else{
		  caledmonth = v.getMonth()+1;
		 }
		 if( v.getDate() < 9 ){
		  caledday = '0'+v.getDate();
		 }else{
		  caledday = v.getDate();
		 }
		 return caledYear + "-" + caledmonth+ '-' + caledday;
		}
		
	
	var handle = 0;
	$(function(){
		createNav("analysis_nav",0);
		getGroup();
		setDate();	
		time();
		
		$("#home").click(function(){ location.href = "/iDOO_CONTROL/chart/index.do" });
		setEl();
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".left,.right").css({
			"height" : getElSize(120)
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_left").css({
			"width" : $(".left").width()
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").not("#intro").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(50),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(40)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding-top" : getElSize(20),
			"padding-bottom" : getElSize(20),
			//"height": getElSize(100)
		});
		
		$(".contentTr, .contentTr2").css({
			"font-size" : getElSize(30)
		});
		
		$(".tableContainer").css({
			"height" : getElSize(1620)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function goGraph(){
		location.href = "${ctxPath}/chart/jigGraph.do"
	};
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/analysis_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/green_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left nav'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td><select id="group"></select>
								<spring:message code="op_period"></spring:message> <input type="date" class="date" id="jig_sdate"> ~ <input type="date" class="date" id="jig_edate"> 
								<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getTableData('jig');">
								<button onclick="goGraph()" id="graph"><spring:message code="graph"></spring:message> </button>
							</td>
						</Tr>		
						<tr>
							<td>
								<div class="tableContainer" >
									<table style="color: white; width: 100%; text-align: center; border-collapse: collapse; " class="tmpTable" id="jigTable">
										<thead>
											<tr style="font-weight: bolder; background-color: rgb(34,34,34)" class="thead">
												<td rowspan="2" style="width: 10%"><spring:message code="device"></spring:message> </td>
												<td rowspan="2" style="width: 5%">JIG</td>
												<td rowspan="2" style="width: 10%">WC</td>
												<td rowspan="2">WC Group</td>  
												<td rowspan="2"><spring:message code="target_op_time"></spring:message></td>
												<td rowspan="2"><spring:message code="total_op_time"></spring:message> </td>
												<td rowspan="2"><spring:message code="number_of_op_day"></spring:message></td>
												<td colspan="4"><spring:message code="avrg_of_day"></spring:message></td>
												<td rowspan="2"><spring:message code="opratio_against_target"></spring:message></td>
											</tr>
											<tr style="font-weight: bolder; background-color: rgb(34,34,34)" class="thead">
												<td><spring:message code="incycle"></spring:message> </td>
												<td><spring:message code="wait"></spring:message></td>
												<td><spring:message code="stop"></spring:message></td>
												<td><spring:message code="noconnection"></spring:message></td>
											</tr>
										</thead>
									</table>
								</div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/selected_green.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back" style="display: none"></div>
	<span id="intro"></span>
</body>
</html>