<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function getBanner(){
		var url = "${ctxPath}/chart/getBanner.do";
		var param = "shopId=" + shopId;
		

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				if(window.localStorage.getItem("banner")=="true"){
					$("#intro").html(data.msg).css({
						"color" : data.rgb,
						"right" : - window.innerWidth
					})
					
					$("#intro").html(data.msg).css({
						"right" : - $("#intro").width()
					})
				}
				
				bannerWidth = $("#intro").width() + getElSize(100); 
				$("#intro").width(bannerWidth);
				$("#banner").val(data.msg);
				$("#color").val(data.rgb);
				
				twinkle();
				bannerAnim();
			}
		});		
	}
	
	var twinkle_opct = false;
	function twinkle(){
		var opacity;
		if(twinkle_opct){
			opacity = 0;
		}else{
			opacity = 1;
		}
		$("#intro").css("opacity",opacity);
		
		twinkle_opct = !twinkle_opct;
		setTimeout(twinkle, 300)
	};
	
	var bannerWidth;
	function bannerAnim(){
		$("#intro").width(bannerWidth - getElSize(10));
		$("#intro").animate({
			"right" : window.innerWidth  - getElSize(100)
		},8000, function(){
			$("#intro").css("right" , - $("#intro").width())
			//$(this).remove();
			bannerAnim();
		});
	};
	
	function chkBanner(){
		if(window.localStorage.getItem("banner")=="true"){
			getBanner();
			$("#intro_back").css("display","block");
		}else{
			$("#intro").html("");
			$("#intro_back").css("display","none");
		}
	};
	
	function cancelBanner(){
		window.localStorage.setItem("banner", "false");
		$("#bannerDiv").css("z-index",-9);
		chkBanner();
	};
	
	function addBanner(){
		var url = "${ctxPath}/chart/addBanner.do";
		var param = "msg=" + encodeURIComponent($("#banner").val()) + 
					"&rgb=" + $("#color").val() + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				window.localStorage.setItem("banner", "true");
				location.reload();
				/* $("#bannerDiv").css("z-index",-9);
				chkBanner(); */
			}
		});
	};
	
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		date.setDate(date.getDate()-2)
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	
	$(function(){
		getLatestDate();
		
		setDate();

		createNav("order_nav", 0);
		
		setEl();
		time();
		
		$("#home").click(function(){ location.href = "/iDOO_CONTROL/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"display" : "none",
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

	
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$(".wrapper").css({
			"width" : $(".menu_right").width(),
			"margin-top" : getElSize(120),
			"position" : "absolute"	
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function getLatestDate(){
		var url = "${ctxPath}/chart/getLatestDate.do";
		
		$.ajax({
			url : url,
			dataType : "text",
			type : "post",
			success : function(data){
				$("#date").val(data);
				getTargetData();	
			}
		});
	};
	
	var className = "";
	var classFlag = true;
	function getTargetData(){
		var url = "${ctxPath}/chart/getTargetData.do";
		
		var param = "shopId=" + shopId + 
					"&date=" + $("#date").val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var  json = data.dataList;
				
				var tr = "<thead>" +
							"<tr>" + 
								"<td rowspan='2'>${device}</td><td colspan='3'>${day}</td><td colspan='3'>${night}</td>" +
							"</tr>" + 
							"<tr>" + 
								"<td style='text-align: center;'>${prdct_cnt}</td><td style='text-align: center;'>${ophour} (h)</td><td style='text-align: center;'>${prdct_per_cycle}</td>" +	
								"<td style='text-align: center;'>${prdct_cnt}</td><td style='text-align: center;'>${ophour} (h)</td><td style='text-align: center;'>${prdct_per_cycle}</td>" +
							"</tr>" + 
						"</thead><tbody>";
				var class_name = "";
				$(json).each(function(idx, data){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					class_name = " ";

					var name = decodeURIComponent(data.name).replace(/\+/gi, " ");
					
					tr += "<tr class='" + className + " contentTr'>" + 
							"<td><input type='hidden' value='" + data.dvcId + "'>" + name + "</td>" +
							"<td><input type='text' value='" + data.tgCntD + "'></td>" +
							"<td><input type='text' value='" + data.tgRunTimeD/60/60 + "'></td>" +
							"<td><input type='text' value='" + data.cntPerCylD + "'></td>" +
							"<td><input type='text' value='" + data.tgCntN + "'></td>" +
							"<td><input type='text' value='" + data.tgRunTimeN/60/60 + "'></td>" +
							"<td><input type='text' value='" + data.cntPerCylN + "'></td>" +
					  "</tr>";
				});
				
		
				tr += "</tbody>";
				
				$(".wrapper").css("z-index" , -99999)
				
				$("#wrapper .targetTable").html(tr);
				$("#wrapper .targetTable td").css("font-size",getElSize(60));
				$("#wrapper .targetTable td input").css({
					"font-size" : getElSize(60),
					"width" : getElSize(200)
				});
				
				$("#wrapper").css("z-index", 999);
				
				$("#wrapper div:last").remove();
				scrolify($("#wrapper .targetTable"), getElSize(1400));
				$("#wrapper div:last").css("overflow", "auto")
				
				

				$(".save_btn").click(function(){
					var img = document.createElement("img");
					img.setAttribute("id", "loading_img");
					img.setAttribute("src", "${ctxPath}/images/load.gif");
					img.style.cssText = "width : " + getElSize(500) + "; " + 
										"position : absolute;" + 
										"z-index : 99999999;" + 
										"border-radius : 50%;"
										
					$("body").prepend(img);
					$("#loading_img").css({
						"top" : (window.innerHeight/2) - ($("#loading_img").height()/2),
						"left" : (window.innerWidth/2) - ($("#loading_img").width()/2)
					});
					
				});
				
				$(".targetCnt").css({
					"font-size" : getElSize(40),
					"width" : getElSize(250),
					"outline" : "none",
					"border" : "none"
				});
				
				$(".span").css({
					"font-size" : getElSize(40),
					"color" : "red",
					"margin-left" : getElSize(10),
					"font-weight" : "bolder"
				});
				
				$(".save_btn").css({
					"background-color" : "white",
					"color" : "black",
					"border-radius" : getElSize(10),
					"font-weight" : "bolder",
					"padding" : getElSize(10)
				});
				
				
				$(".tdisable").each(function(idx, data){
					this.disabled = true;
					this.value = "";
				});
				
				$(".tdisable").css({
					"background-color" : "rgba(	4,	238,	91,0.5)"
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222",
					"font-size" : getElSize(40)
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232",
					"font-size" : getElSize(40)
				});
				
			}
		});
	};

	function copyRow(){
		$(".contentTr").each(function(idx, data){
			$(data).children("td:nth(4)").children("input").val($(data).children("td:nth(1)").children("input").val());
			$(data).children("td:nth(5)").children("input").val($(data).children("td:nth(2)").children("input").val())
			$(data).children("td:nth(6)").children("input").val($(data).children("td:nth(3)").children("input").val())
		});
	};
	
	var valueArray = [];
	function saveRow(){
		valueArray = [];
		$(".contentTr").each(function(idx, data){
			var obj = new Object();
			obj.dvcId = $(data).children("td:nth(0)").children("input").val();
			obj.tgCyl = $(data).children("td:nth(1)").children("input").val();
			obj.tgRunTime = $(data).children("td:nth(2)").children("input").val()*3600;
			obj.tgDate = getToday2();
			obj.type = 2;
			obj.cntPerCyl = $(data).children("td:nth(3)").children("input").val()
			
			valueArray.push(obj);
			
			var obj = new Object();
			obj.dvcId = $(data).children("td:nth(0)").children("input").val();
			obj.tgCyl = $(data).children("td:nth(4)").children("input").val();
			obj.tgRunTime = $(data).children("td:nth(5)").children("input").val()*3600;
			obj.tgDate = getToday2();
			obj.type = 1;
			obj.cntPerCyl = $(data).children("td:nth(6)").children("input").val()
			
			valueArray.push(obj)
			
		});
		
		var obj = new Object();
		obj.val = valueArray;
		
		var url = "${ctxPath}/chart/addTargetCnt.do";
		var param = "val=" + JSON.stringify(obj);
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function (data){
				console.log(data)
				if(data=="success"){
					alert("${save_ok}");
				}		
			}
		});
	};
	
	function getToday2(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		return year + "-" + month + "-" + day	
	};
	
	</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="bannerDiv">
	<Center>
		<input type="text" id="banner" size="100"><input type="color" id="color"><br>
		<button onclick="addBanner();">적용</button>
		<button onclick="cancelBanner();">미적용</button>
	</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/order_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<table style="width: 100%; float: right;">
						<Tr>
							<Td style="color: white; text-align: right;">
								<input type="date" id="date"> 
								<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getTargetData()">
								<button onclick="saveRow()"><spring:message code="save"></spring:message> </button> 
								<button onclick="copyRow()"><spring:message code="copy_data"></spring:message></button>
							</Td>
						</Tr>
					</table>
					<div id="wrapper" class="wrapper">
						<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" id="target" class="targetTable" border="1">
						</table>
					</div>
				</td>
				
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	