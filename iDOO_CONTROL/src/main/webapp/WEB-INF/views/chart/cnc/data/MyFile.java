package com.unomic.cnc.data;

public class MyFile
{
	private String path;
	private String name;
	private long size;
	private boolean isDir;
	
	public MyFile(String path, String name, long size, boolean isDir)
	{
		this.path = path;
		this.name = name;
		this.size = size;
		this.isDir = isDir;
	}
	
	public String getPath()
	{
		return path;
	}
	public void setPath(String path)
	{
		this.path = path;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public long getSize()
	{
		return size;
	}
	public void setSize(long size)
	{
		this.size = size;
	}
	public boolean isDir()
	{
		return isDir;
	}
	public void setDir(boolean isDir)
	{
		this.isDir = isDir;
	}
}
