<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function getBanner(){
		var url = "${ctxPath}/chart/getBanner.do";
		var param = "shopId=" + shopId;
		

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				if(window.localStorage.getItem("banner")=="true"){
					$("#intro").html(data.msg).css({
						"color" : data.rgb,
						"right" : - window.innerWidth
					})
					
					$("#intro").html(data.msg).css({
						"right" : - $("#intro").width()
					})
				}
				
				bannerWidth = $("#intro").width() + getElSize(100); 
				$("#intro").width(bannerWidth);
				$("#banner").val(data.msg);
				$("#color").val(data.rgb);
				
				twinkle();
				bannerAnim();
			}
		});		
	}
	
	var twinkle_opct = false;
	function twinkle(){
		var opacity;
		if(twinkle_opct){
			opacity = 0;
		}else{
			opacity = 1;
		}
		$("#intro").css("opacity",opacity);
		
		twinkle_opct = !twinkle_opct;
		setTimeout(twinkle, 300)
	};
	
	var bannerWidth;
	function bannerAnim(){
		$("#intro").width(bannerWidth - getElSize(10));
		$("#intro").animate({
			"right" : window.innerWidth  - getElSize(100)
		},8000, function(){
			$("#intro").css("right" , - $("#intro").width())
			//$(this).remove();
			bannerAnim();
		});
	};
	
	function chkBanner(){
		if(window.localStorage.getItem("banner")=="true"){
			getBanner();
			$("#intro_back").css("display","block");
		}else{
			$("#intro").html("");
			$("#intro_back").css("display","none");
		}
	};
	
	function cancelBanner(){
		window.localStorage.setItem("banner", "false");
		$("#bannerDiv").css("z-index",-9);
		chkBanner();
	};
	
	function addBanner(){
		var url = "${ctxPath}/chart/addBanner.do";
		var param = "msg=" + encodeURIComponent($("#banner").val()) + 
					"&rgb=" + $("#color").val() + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				window.localStorage.setItem("banner", "true");
				location.reload();
				/* $("#bannerDiv").css("z-index",-9);
				chkBanner(); */
			}
		});
	};
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	function caldate(day){
		 var caledmonth, caledday, caledYear;
		 var loadDt = new Date();
		 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
		 
		 caledYear = v.getFullYear();
		 
		 if( v.getMonth() < 9 ){
		  caledmonth = '0'+(v.getMonth()+1);
		 }else{
		  caledmonth = v.getMonth()+1;
		 }
		 if( v.getDate() < 9 ){
		  caledday = '0'+v.getDate();
		 }else{
		  caledday = v.getDate();
		 }
		 return caledYear + "-" + caledmonth+ '-' + caledday;
		}
		
	
	var handle = 0;

	function getGroup(){
		var url = "${ctxPath}/chart/getPrdNoList.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
				});
				
				$("#prdNo").html(option).change(getDvcListByPrdNo)
				 
				$("#chkTy").html("<option value='ALL' >${total}</option>" + getCheckType());
				
				getDvcListByPrdNo();
			}
		});
	};
	
	function chkFaulty(el, ty){
		if(ty=="select"){
			var val = $(el).val();
			
			if(val==2){
				$(el).parent("Td").parent("tr").css("background-color" , "red");
			}else{
				if($(el).parent("Td").parent("tr").hasClass("row1")){
					$(el).parent("Td").parent("tr").css("background-color" , "#222222");	 
				}else{
					$(el).parent("Td").parent("tr").css("background-color" , "#323232");
				}
			};	
		}else{
			var min = Number($(el).parent("td").parent("tr").children("td:nth(7)").html());
			var max = Number($(el).parent("td").parent("tr").children("td:nth(8)").html());
			var val = Number($(el).val());
			
			if(val>max || val < min){
				$(el).parent("Td").parent("tr").css("background-color" , "red");
			}else{
				if($(el).parent("Td").parent("tr").hasClass("row1")){
					$(el).parent("Td").parent("tr").css("background-color" , "#222222");	 
				}else{
					$(el).parent("Td").parent("tr").css("background-color" , "#323232");
				}
			}
		}
	};
	
	function getCheckType(){
		var url = "${ctxPath}/chart/getCheckType.do";
		var param = "codeGroup=INSPPNT";
		
		var option = "";
		
		$.ajax({
			url : url,
			data : param,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					var codeName;
					if(decode(data.codeName)=="입고검사"){
						codeName = "${in_check}";
					}else{
						codeName = decode(data.codeName);
					}
					
					option += "<option value='" + data.id + "'>" + codeName + "</option>"; 
				});
				
				chkTy = "<select>" + option + "</select>";
			}
		});
		
		return option;
	};
	
	function getDvcListByPrdNo(){
		var url = "${ctxPath}/chart/getDvcListByPrdNo.do";
		var param = "prdNo=" + $("#prdNo").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				var option = "";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.dvcId + "'>" + decode(data.name) + "</option>"; 
				});
				
				$("#dvcId").html(option).val(json[0].dvcId);
				//getCheckList();
			}
		});
	};
	
	var className = "";
	var classFlag = true;
	var result = "<select style='font-size:" + getElSize(40) + "' onchange='chkFaulty(this, \"select\")'><option value='0' >${selection}</option><option value='1'>${ok}</option><option value='2'>${faulty}</option></select>";
	function getCheckList(){
		classFlag = true;
		var url = "${ctxPath}/chart/getCheckList.do";
		var sDate = $(".date").val();
		
		var param = "prdNo=" + $("#prdNo").val() + 
					"&chkTy=" + $("#chkTy").val() +
					"&checkCycle=" + $("#checkCycle").val() + 
					"&date=" + sDate + 
					"&ty=" + $("#workTime").val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var tr = "<tbody>";
						
				alarmList = [];
				$(json).each(function(idx, data){
					if(data.seq!="."){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						
						var attrTy;
						if(data.attrTy==1){
							//attrTy = "정성검사";
							attrTy = "${js_check}";
						}else{
							//attrTy = "정량검사";
							attrTy = "${jr_check}";
						};
					
						var date;
						if(data.date==""){
							date = $(".date").val();
						}else{
							date = data.date;
						};
						
						var resultVal, resultVal2, resultVal3, resultVal4;
						if(data.attrTy==1){
							resultVal = resultVal2 = resultVal3 = resultVal4 = result;
						}else{
							resultVal = "<input type='text' value='" + data.result + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
							resultVal2 = "<input type='text' value='" + data.result2 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
							resultVal3 = "<input type='text' value='" + data.result3 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
							resultVal4 = "<input type='text' value='" + data.result4 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
						};
						
						//console.log(data.chkTy)
						var chkTy;
						if(decodeURIComponent(data.chkTy).replace(/\+/gi, " ")=="입고검사"){
							chkTy = "${in_check}"	
						}else{
							chkTy = decodeURIComponent(data.chkTy).replace(/\+/gi, " ");
						}
						
						tr = "<tr class='contentTr " + className + "' id='tr" + data.id + "'>" +
									"<td id='t" + data.listId + "'>" + data.prdNo + "</td>" +
									"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" + 
									"<td>" + chkTy + "</td>" +
									"<td>" + attrTy + "</td>" + 
									"<td>" + decodeURIComponent(data.attrNameKo).replace(/\+/gi, " ") + "</td>" + 
									"<td>" + decodeURIComponent(data.spec).replace(/\+/gi, " ") + "</td>" + 
									"<td>" + data.target + "</td>" + 
									"<td>" + data.low + "</td>" + 
									"<td>" + data.up + "</td>" +
									"<td>" + data.measurer + "</td>" +
									"<td id='result" + data.id + "'>" + resultVal + "</td>" +
									"<td id='result2" + data.id + "'>" + resultVal2 + "</td>" +
									"<td id='result3" + data.id + "'>" + resultVal3 + "</td>" +
									"<td id='result4" + data.id + "'>" + resultVal4 + "</td>" +
									"<td id='fResult" + data.id + "'><select style='font-size : " + getElSize(40) + "'><option value='1'>${ok}</option><option value='2'>${faulty}</option></select></td>" +
									"<td><input type='date' value='" + date + "' style='font-size:" + getElSize(40) + "'></td>" +
									"<td id='chkCycle" + data.id + "'>" + $("#checkCycle option:selected").html() + "</td>" +
									"<td id='workTy" + data.id + "'><select style='font-size : " + getElSize(40) + "'><option value='2'>${day}</option><option value='1'>${night}</option></select></td>" +
							"</tr>";		
							
						$(".alarmTable").append(tr).css({
							"font-size": getElSize(40),
						});		
						
						var checkCycle = data.checkCycle;
						if(checkCycle==0) checkCycle = $("#checkCycle").val();
						
						$("#chkCycle" + data.id + " select option[value=" + checkCycle + "]").attr('selected','selected');
						$("#workTy" + data.id + " select option[value=" + $("#workTime").val() + "]").attr('selected','selected');
						$("#fResult" + data.id + " select option[value=" + data.fResult + "]").attr('selected','selected');
						
						if(data.attrTy==1){
							$("#result" + data.id + " select option[value=" + data.result + "]").attr('selected','selected');
							$("#result2" + data.id + " select option[value=" + data.result2 + "]").attr('selected','selected');
							$("#result3" + data.id + " select option[value=" + data.result3 + "]").attr('selected','selected');
							$("#result4" + data.id + " select option[value=" + data.result4 + "]").attr('selected','selected');
							
							if(data.result==2){
								alarmList.push("#tr" + data.id);
							}	
						}else if(Number(data.result) > Number(data.up) || Number(data.result) < Number(data.low)){
							alarmList.push("#tr" + data.id);
						}
						
						
					}
				});
				
				$(".alarmTable").append("</tbody>");
				//$(".alarmTable").css("width", "100%");
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(35),
					"border": getElSize(5) + "px solid black"
				});
				
				/* $("#wrapper").css({
					"height" :getElSize(1550),
					"width" : $(".right").width(),
					"overflow" : "auto"
				}); */
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("#wrapper div:last").remove();
				//scrolify($('.alarmTable'), getElSize(1350));
				$("#wrapper div:last").css("overflow", "auto");
				
				$(alarmList).each(function(idx,data){
					$(data).css("background-color" , "red");
				});
			}
		});
	};
	
	$(function(){
		createNav("quality_nav", 0);
		getGroup();
		bindEvt2();
		$(".excel").click(csvSend);
		setEl();
		setDate();
		
		time();
		
		$("#home").click(function(){ location.href = "/iDOO_CONTROL/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function calcTimeDiff(start, end){
		var startHour = Number(start.substr(0,2));
		var startMinute = Number(start.substr(3,2));
		var endHour = Number(end.substr(0,2));
		var endMinute = Number(end.substr(3,2));
		
		var startTime = new Date($("#startDate_form").val() + "," + $("#startTime_form").val());
		var endTime = new Date($("#endDate_form").val() + "," + $("#endTime_form").val());
		
		var timeDiff = (endTime.getTime() - startTime.getTime()) / 1000 /60;
		
		return timeDiff;
	}
	
	
	function bindEvt2(){

	};
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	var csvOutput;
	function csvSend(){
		var sDate, eDate;
		
		sDate = $("#sDate").val();
		eDate = $("#eDate").val();
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = eDate;
		f.submit(); 
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"display" : "none",
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

	
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(25),
			"padding" : getElSize(15),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(50),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#table2").css({
			"width" : getElSize(3800)
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		
		$(".alarmTable td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(35),
			"border": getElSize(5) + "px solid black"
		});
		
		$("#wrapper").css({
			"height" :getElSize(1550),
			"width" : $(".right").width(),
			"overflow" : "auto"
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function goGraph(){
		location.href = "${ctxPath}/chart/jigGraph.do"
	};
	
	var valueArray = [];
	function saveRow(){
		valueArray = [];
		$(".contentTr").each(function(idx, data){
			var obj = new Object();
			
			obj.chkId = data.id.substr(2);
			obj.id = $(data).children("td:nth(0)").attr("id").substr(1);
			var result = $(data).children("td:nth(10)").children("input").val();
			var result2 = $(data).children("td:nth(11)").children("input").val();
			var result3 = $(data).children("td:nth(12)").children("input").val();
			var result4 = $(data).children("td:nth(13)").children("input").val();
			
			if(typeof(result)=="undefined"){
				result = $(data).children("td:nth(10)").children("select").val();
				result2 = $(data).children("td:nth(11)").children("select").val();
				result3 = $(data).children("td:nth(12)").children("select").val();
				result4 = $(data).children("td:nth(13)").children("select").val();
			}
			obj.result = result;
			obj.result2 = result2;
			obj.result3 = result3;
			obj.result4 = result4;
			
			obj.workTy = $(data).children("td:nth(17)").children("select").val();
			obj.date = $(data).children("td:nth(15)").children("input").val();
			obj.fResult = $(data).children("td:nth(14)").children("select").val(); 
			obj.checker = $("#checker").val();
			obj.chkCycle = $("#checkCycle").val(); 
				
			valueArray.push(obj);
		});
		
		var obj = new Object();
		obj.val = valueArray;
		
		var url = "${ctxPath}/chart/addCheckStandardList.do";
		var param = "val=" + JSON.stringify(obj);
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType :"text",
			success : function(data){
				if(data=="success") {
					alert ("${save_ok}");
					getCheckList();
				}
			}
		});
	};

	function addFaulty(el){
		var prdNo = $("#prdNo").val();
		var cnt = 0;
		var url = "${ctxPath}/chart/addFaulty.do?addFaulty=true&prdNo=" + prdNo + "&cnt="+ cnt;
		
		location.href = url;
	};
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="bannerDiv">
	<Center>
		<input type="text" id="banner" size="100"><input type="color" id="color"><br>
		<button onclick="addBanner();">적용</button>
		<button onclick="cancelBanner();">미적용</button>
	</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<tr>
							<Td><spring:message  code="prd_no"></spring:message></Td>
							<Td><select id="prdNo"></select ></Td><Td><spring:message  code="device"></spring:message></td>
							<td ><select id="dvcId"></select></td> <Td><spring:message  code="check_type"></spring:message></Td>
							<Td><select id="chkTy"></select></Td>
							<Td rowspan="2"><img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getCheckList()"> </Td>
							<Td rowspan="2" style="vertical-align: bottom;">
								<button onclick="addFaulty()"><spring:message  code="add_faulty"></spring:message></button>
								<button onclick="saveRow();"><spring:message  code="save"></spring:message></button>
							</Td>
						</tr>
						<tr>
							<Td><spring:message  code="check_date"></spring:message></Td>
							<Td><input type="date" class='date'></Td>
							<Td><spring:message  code="checker"></spring:message></Td><Td><input type="text" id="checker"></Td>
							<Td ><spring:message  code="check_cycle"></spring:message></Td>
							<Td>
								<select id="checkCycle"><option value="1"><spring:message  code="first_prdct"></spring:message></option><option value="2"><spring:message  code="mid_prdct"></spring:message></option><option value="3"><spring:message  code="last_prdct"></spring:message></option></select>
								<select id="workTime"><option value="2"><spring:message  code="day"></spring:message></option><option value="1"><spring:message  code="night"></spring:message></option></select>
							</Td>
						</tr>				
					</table> 
					<div id="wrapper">
						<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1" id="table2">
							<thead>
								<Tr style="background-color: #222222">
									<td><spring:message  code="prd_no"></spring:message></td>
									<Td><spring:message  code="device"></spring:message></Td>
									<Td><spring:message  code="check_type"></spring:message></Td>
									<Td><spring:message  code="character_type"></spring:message></Td>
									<Td><spring:message  code="character_name"></spring:message></Td>
									<td><spring:message  code="drawing"></spring:message>Spec</td>
									<Td><spring:message  code="target_val"></spring:message></Td>
									<Td><spring:message  code="min_val"></spring:message></Td>
									<Td><spring:message  code="max_val"></spring:message></Td>
									<Td><spring:message  code="measurer"></spring:message></Td>
									<Td><spring:message  code="check_result"></spring:message></Td>
									<Td><spring:message  code="check_result"></spring:message></Td>
									<Td><spring:message  code="check_result"></spring:message></Td>
									<Td><spring:message  code="check_result"></spring:message></Td>
									<Td><spring:message  code="result"></spring:message></Td>
									<Td><spring:message  code="check_date"></spring:message></Td>
									<Td><spring:message  code="check_cycle"></spring:message></Td>
									<Td><spring:message  code="division"></spring:message></Td>
								</Tr>
							</thead>
						</table>
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left '>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	